#!/bin/bash

if [[ "$UID" != 0 ]]; then
	/bin/sudo "$0" "$@"
	exit
fi


# Lock screen to prevent interruption
# xflock4 &
dbus-send --type=method_call --dest=org.gnome.ScreenSaver \
    /org/gnome/ScreenSaver org.gnome.ScreenSaver.Lock &

for dev in /dev/mapper/luks-*; do # Wipe all mapped LUKS devices
	cryptsetup erase "$dev"
done

sync # Sync to ensure new data is written
fstrim -va # TRIM to encourage the drive to zero old blocks

for dev in /dev/mapper/luks-*; do # Wipe twice because we're paranoid
	cryptsetup erase "$dev"   # Really this is just an excuse to sync and TRIM again
done                              # And to give the drive slightly more time to zero blocks

sync
fstrim -va

for f in /dev/*; do #
	echo "Panic handler: Wipe operation completed" > "$dev"
done

sync
fstrim -va

# Shutdown using normal shutdown process. This has 2 benefits:
# - It gives the drive more time to zero old blocks
# - It allows the system to cleanly wipe all RAM instead of relying on power loss
shutdown now
