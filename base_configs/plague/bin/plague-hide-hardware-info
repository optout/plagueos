#!/usr/bin/bash

# Enforce strict error handling:
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
#   shopt -s failglob
#   ^ Disabled due to annoying warnings I can't seemingly suppress, perhaps it's for the best.
shopt -s inherit_errexit

# Enable extended globbing/pattern matching to support more refined pattern matching capabilities:
shopt -s extglob

handle_script_failure() {
    if [[ ${1} -ne 0 ]]; then
        echo "ERROR: Received error signal \"${1}\" in the file \"${2}\" while executing the command" \
             "\"${BASH_COMMAND}\" on line number \"${3}\"! Exiting..."
    fi
}

trap 'handle_script_failure $? $BASH_SOURCE $LINENO' ERR
trap 'handle_script_failure $? $BASH_SOURCE $LINENO' EXIT

# Ensure the user executing this script is root as privileged operations are required:
if [[ $(id --user) -ne 0 ]]; then
    echo "ERROR: This script must be run as root!"
    exit 1
fi


# Miscellaneous settings:
## Cause the hide-hardware-info script to run in a more verbose "debug" mode:
DEBUG_MODE="false"

# Default whitelist group settings:
## Enable utilization of the /proc/cpuinfo whitelist group:
WHITELIST_GROUP_CPUINFO="true"
## Name of the group used for our /proc/cpuinfo whitelisting:
CPUINFO_GROUP_NAME="cpuinfo"
## Enable utilization of the /sys whitelist group:
WHITELIST_GROUP_SYSFS="true"
## Name of the group used for our /sys whitelisting:
SYSFS_GROUP_NAME="sysfs"

# Default hardening exception settings:
## Enable exceptions required to support SELinux:
EXCEPTIONS_SELINUX="false"
## Enable exceptions required to support non-whitelist group members accessing resources pertaining to specifc hardware
## |    sensor data, CPU temperature, fan speed, etc:
EXCEPTIONS_HARDWARE_SENSORS="false"
## Enable exceptions required to support non-whitelist group members utilizing GPU devices with software such as Wayland
## |    or X11:
EXCEPTIONS_WAYLAND_OR_X11_GPU="true"
## Enable exceptions required to support non-whitelist group members utilizing a laptop's built-in input devices with
## |    Wayland or X11:
EXCEPTIONS_WAYLAND_OR_X11_INPUT_LAPTOP_BUILTIN="true"
## Enable exceptions required to support non-whitelist group members utilizing USB input devices with Wayland or X11:
EXCEPTIONS_WAYLAND_OR_X11_INPUT_USB="true"
## Enable exceptions required to support non-whitelist group members utilizing VIRTIO input devices with Wayland or X11:
EXCEPTIONS_WAYLAND_OR_X11_INPUT_VIRTIO="false"
## Enable exceptions required to support non-whitelist group members utilizing virtual input devices provided by the
## |    "KLOAK" program with Wayland or X11:
EXCEPTIONS_WAYLAND_OR_X11_INPUT_KLOAK="false"
## Enable exceptions required to support non-whitelist group members utilizing GPUs with Firefox, these exceptions are
## |    unreasonably excessive due to the questionable device detection mechanism utilized by Firefox:
EXCEPTIONS_WAYLAND_OR_X11_FIREFOX_GPU="false"
## Enable exceptions required to support non-whitelist group members running libvirt "QEMU://user-session" VMs:
EXCEPTIONS_LIBVIRT_QEMU_USER_SESSION="true"

# Load configuration files allowing default settings to be overridden:
if [[ -d /etc/hide-hardware-info.d/ && ! -z $(ls --almost-all /etc/hide-hardware-info.d/*.conf) ]]; then
    for CONFIG_FILE in /etc/hide-hardware-info.d/*.conf; do
        source "${CONFIG_FILE}"
    done
fi

# Activate a more verbose "debugging" mode if "DEBUG_MODE" is enabled:
if [[ ${DEBUG_MODE} = 'true' ]]; then
    set -o xtrace
fi


# Implements supported whitelist group functionality:
implement_whitelist() {
    case "${1}" in
        "${CPUINFO_GROUP_NAME}") ;&
        "${SYSFS_GROUP_NAME}")
            WHITELIST_GROUP="${1}"
            WHITELIST_FILE_PATH="${2}"
            ;;
        *)
            echo "ERROR: Attempted to utilize unknown whitelist group \"${1}\"! Exiting..."
            exit 1
            ;;
    esac

    # Check if the relevant whitelist group exists:
    if grep --quiet '^'"${WHITELIST_GROUP}"':' /etc/group; then
        if [[ ${WHITELIST_GROUP} = ${SYSFS_GROUP_NAME} ]]; then
            # Errors are ignored due to certain resources under /sys/ not supporting permission changes:
            chgrp --recursive "${WHITELIST_GROUP}" "${WHITELIST_FILE_PATH}" || /usr/bin/true
        else
            chgrp --recursive "${WHITELIST_GROUP}" "${WHITELIST_FILE_PATH}"
        fi
    else
        echo "ERROR: The \"${1}\" group does not exist! Exiting..."
        exit 1
    fi
}


# Implements the base hardening configuration:
implement_base_hardening() {
    echo "INFO: Attempting to apply the base hardening configuration..."

    declare -a PATHS_TO_HARDEN=(
        "/proc/bus/"
        "/proc/cpuinfo"
        "/proc/scsi/"
        "/sys/"
    )

    for PATH_TO_HARDEN in "${PATHS_TO_HARDEN[@]}"; do
        if [[ -e "${PATH_TO_HARDEN}" ]]; then
            case "${PATH_TO_HARDEN}" in
                '/proc/cpuinfo')
                    if [[ ${WHITELIST_GROUP_CPUINFO} = 'true' ]]; then
                        implement_whitelist cpuinfo "${PATH_TO_HARDEN}"
                        chmod o-rwx "${PATH_TO_HARDEN}"
                    else
                        chmod go-rwx "${PATH_TO_HARDEN}"
                        echo "WARN: The whitelist group for /proc/cpuinfo was not utilized, any group members will" \
                             "be unable to bypass the relevant hardening restrictions."
                    fi
                    ;;
                '/sys/')
                    if [[ ${WHITELIST_GROUP_SYSFS} = 'true' ]]; then
                        implement_whitelist sysfs "${PATH_TO_HARDEN}"
                        # Errors are ignored due to certain resources under /sys/ not supporting permissions changes:
                        chmod --recursive o-rwx "${PATH_TO_HARDEN}" || /usr/bin/true
                    else
                        # Errors are ignored due to certain resources under /sys/ not supporting permissions changes:
                        chmod --recursive go-rwx "${PATH_TO_HARDEN}" || /usr/bin/true
                        echo "WARN: The whitelist group for /sys/ resources was not utilized, any group members will" \
                             "be unable to bypass the relevant hardening restrictions."
                    fi
                    ;;
                *)
                    chmod --recursive go-rwx "${PATH_TO_HARDEN}"
            esac
        fi
    done

    echo -e "SUCCESS: The base hardening configuration was successfully applied.\n"
}


hardening_exceptions_for_selinux() {
    # Check for the presence of SELinux, if it exists then determine if any relevant hardening exceptions apply:
    if [[ -d /sys/fs/selinux/ ]]; then
        if [[ ${EXCEPTIONS_SELINUX} = 'true' ]]; then
            echo "INFO: Attempting to configure hardening exceptions for SELinux..."

            # Currently this simply re-applies the default DAC permissions for "other" users that exists on Fedora:
            chmod o+rx /sys/            \
                       /sys/fs/         \
                       /sys/fs/selinux/

            find /sys/fs/selinux/ -type d -exec chmod o+rx {} \;
            find /sys/fs/selinux/avc/                 -type f -exec chmod o+r {} \;
            find /sys/fs/selinux/booleans/            -type f -exec chmod o+r {} \;
            find /sys/fs/selinux/class/               -type f -exec chmod o+r {} \;
            find /sys/fs/selinux/initial_contexts/    -type f -exec chmod o+r {} \;
            find /sys/fs/selinux/policy_capabilities/ -type f -exec chmod o+r {} \;
            find /sys/fs/selinux/ss/                  -type f -exec chmod o+r {} \;

            chmod o+rw /sys/fs/selinux/access
            chmod o+r  /sys/fs/selinux/checkreqprot
            chmod o+rw /sys/fs/selinux/context
            chmod o+rw /sys/fs/selinux/create
            chmod o+r  /sys/fs/selinux/deny_unknown
            chmod o+r  /sys/fs/selinux/enforce
            chmod o+rw /sys/fs/selinux/member
            chmod o+r  /sys/fs/selinux/mls
            chmod o+rw /sys/fs/selinux/null
            chmod o+r  /sys/fs/selinux/policy
            chmod o+r  /sys/fs/selinux/policyvers
            chmod o+r  /sys/fs/selinux/reject_unknown
            chmod o+rw /sys/fs/selinux/relabel
            chmod o+r  /sys/fs/selinux/status
            chmod o+rw /sys/fs/selinux/user
            chmod o+w  /sys/fs/selinux/validatetrans

            echo -e "SUCCESS: Hardening exceptions for SELinux were applied successfully.\n"
        else
            echo "WARN: SELinux detected but no relevant hardening exceptions to support it have been configured."
        fi
    fi
}


hardening_exceptions_for_hardware_sensors() {
    # If enabled configure hardening exceptions allowing non-whitelist group members access to specific hardware sensor
    # |     data:
    if [[ ${EXCEPTIONS_HARDWARE_SENSORS} = 'true' ]]; then
        echo "INFO: Attempting to configure hardening exceptions for hardware sensors..."

        # Check if any devices belonging to the "hwmon" subsystem exist:
        if [[ -d /sys/class/hwmon/ && ! -z $(ls --almost-all /sys/class/hwmon/) ]]; then
            chmod o+rx /sys/             \
                       /sys/class/       \
                       /sys/class/hwmon/

            if [[ -d /sys/bus/i2c/ ]]; then
                chmod o+rx /sys/bus/             \
                           /sys/bus/i2c/         \
                           /sys/bus/i2c/devices/
            fi

            declare -a HWMON_DEVICE_PATHS=(/sys/class/hwmon/*)

            for HWMON_DEVICE_PATH in "${HWMON_DEVICE_PATHS[@]}"; do
                HWMON_DEVICE_REALPATH="$(realpath "${HWMON_DEVICE_PATH}")"
                declare -a HWMON_DEVICE_FILES=("${HWMON_DEVICE_REALPATH}"/*)

                # Check the current HWMON_DEVICE's directory for known filename matches and set their permissions:
                for HWMON_DEVICE_FILE_PATH in "${HWMON_DEVICE_FILES[@]}"; do
                    HWMON_DEVICE_FILENAME="$(basename "${HWMON_DEVICE_FILE_PATH}")"
                    case "${HWMON_DEVICE_FILENAME}" in
                        'fan'+([[:digit:]])'_input')       ;&
                        'name')                            ;&
                        'temp'+([[:digit:]])'_crit')       ;&
                        'temp'+([[:digit:]])'_crit_alarm') ;&
                        'temp'+([[:digit:]])'_input')      ;&
                        'temp'+([[:digit:]])'_label')      ;&
                        'temp'+([[:digit:]])'_max')        ;&
                        'pwm'+([[:digit:]]))
                            chmod o+r "${HWMON_DEVICE_FILE_PATH}"
                            ;;
                    esac
                done

                # Ensure the DAC permissions for any directories preceding that of the current HWMON_DEVICE's are
                # |     appropriately relaxed.
                chmod o+rx "${HWMON_DEVICE_REALPATH}"/
                PRECEDING_DIRECTORIES="$(echo "${HWMON_DEVICE_REALPATH}" | grep -o '/' | wc -l)"
                while [[ ${PRECEDING_DIRECTORIES} -gt 2 ]]; do
                    HWMON_DEVICE_PRECEDING_DIR="$(echo "${HWMON_DEVICE_REALPATH}" | cut --delimiter='/' --fields=1-${PRECEDING_DIRECTORIES})"
                    chmod o+rx "${HWMON_DEVICE_PRECEDING_DIR}"/
                    ((--PRECEDING_DIRECTORIES))
                done
            done
        else
            echo "WARN: Hardening exceptions for hardware sensors are enabled but no such sensors were detected."
        fi

        echo -e "SUCCESS: Hardening exceptions for hardware sensors were applied successfully.\n"
    fi
}


hardening_exceptions_for_video_devices_on_wayland_or_x11() {
    # If enabled configure hardening exceptions allowing non-whitelist group members access to GPU resources when using
    # |     software such as Wayland or X11:
    if [[ ${EXCEPTIONS_WAYLAND_OR_X11_GPU} = 'true' ]]; then
        echo "INFO: Attempting to configure hardening exceptions for utilizing GPU device/s with Wayland or X11..."

        # Check if any devices belonging to the "drm" subsystem exist:
        if [[ -d /sys/class/drm/ && ! -z $(ls --almost-all /sys/class/drm/) ]]; then
            chmod o+rx /sys/                    \
                       /sys/bus/                \
                       /sys/bus/pci/            \
                       /sys/bus/pci/devices/    \
                       /sys/class/              \
                       /sys/class/drm/          \
                       /sys/dev/                \
                       /sys/dev/char/           \
                       /sys/devices/            \
                       /sys/devices/pci0000:00/

            if [[ -d /sys/devices/i915/ ]]; then
                chmod o+rx /sys/devices/i915/
                chmod o+r /sys/devices/i915/uevent
            fi

            declare -a GPU_DEVICES=(/sys/class/drm/*)
            GPU_DEVICE_REGEX='^/sys/devices/pci0000:00/[[:digit:]]+:[[:alnum:]]+:[[:alnum:]]+.[[:digit:]]+/*.*/drm/card[[:digit:]]+$'
            for GPU_DEVICE_PATH in "${GPU_DEVICES[@]}"; do
                GPU_DEVICE_REALPATH="$(realpath "${GPU_DEVICE_PATH}")"
                if [[ ${GPU_DEVICE_REALPATH} =~ ${GPU_DEVICE_REGEX} ]]; then
                    GPU_DEVICE_ROOTPATH="$(realpath "${GPU_DEVICE_REALPATH}"/device/)"
                    chmod o+rx "${GPU_DEVICE_ROOTPATH}"/                                                          \
                               "${GPU_DEVICE_ROOTPATH}"/drm/                                                      \
                               "${GPU_DEVICE_ROOTPATH}"/drm/card+([[:digit:]])/                                   \
                               "${GPU_DEVICE_ROOTPATH}"/drm/card+([[:digit:]])/card+([[:digit:]])-+([[:graph:]])/ \
                               "${GPU_DEVICE_ROOTPATH}"/drm/render+([[:alnum:]])/
                    chmod o+r "${GPU_DEVICE_ROOTPATH}"/device                                                          \
                              "${GPU_DEVICE_ROOTPATH}"/uevent                                                          \
                              "${GPU_DEVICE_ROOTPATH}"/vendor                                                          \
                              "${GPU_DEVICE_ROOTPATH}"/drm/card+([[:digit:]])/uevent                                   \
                              "${GPU_DEVICE_ROOTPATH}"/drm/card+([[:digit:]])/card+([[:digit:]])-+([[:graph:]])/uevent \
                              "${GPU_DEVICE_ROOTPATH}"/drm/render+([[:alnum:]])/uevent

                    # Errors here are ignored due to an inconsistency between drivers in whether these resources exist:
                    chmod --silent o+r "${GPU_DEVICE_ROOTPATH}"/subsystem_device \
                                       "${GPU_DEVICE_ROOTPATH}"/subsystem_vendor || /usr/bin/true

                    # Ensure the DAC permissions for any directories preceding that of the current INPUT_DEVICE are
                    # |     appropriately relaxed.
                    PRECEDING_DIRECTORIES="$(echo "${GPU_DEVICE_ROOTPATH}" | grep -o '/' | wc -l)"
                    while [[ ${PRECEDING_DIRECTORIES} -gt 4 ]]; do
                        GPU_DEVICE_PRECEDING_DIR="$(echo "${GPU_DEVICE_ROOTPATH}" | cut --delimiter='/' --fields=1-${PRECEDING_DIRECTORIES})"
                        chmod o+rx "${GPU_DEVICE_PRECEDING_DIR}"/
                        ((--PRECEDING_DIRECTORIES))
                    done
                fi
            done
        else
            echo "WARN: Hardening exceptions for GPU resources are enabled but no such devices were detected."
        fi

        echo -e "SUCCESS: Hardening exceptions for utilizing GPU device/s with Wayland or X11 were applied" \
                "successfully.\n"
    fi
}


hardening_exceptions_for_laptop_builtin_input_on_wayland_or_x11() {
    # If enabled configure hardening exceptions allowing non-whitelist group members access to a laptops built-in input
    # |     device/s when using Wayland or X11:
    if [[ ${EXCEPTIONS_WAYLAND_OR_X11_INPUT_LAPTOP_BUILTIN} = 'true' ]]; then
        echo "INFO: Attempting to configure hardening exceptions for using a laptop's built-in input device/s with" \
             "Wayland or X11..."

        if [[ -d /sys/devices/platform/i8042/ ]]; then
            chmod o+rx /sys/                        \
                       /sys/class/                  \
                       /sys/class/input/            \
                       /sys/dev/                    \
                       /sys/dev/char/               \
                       /sys/devices/                \
                       /sys/devices/platform/       \
                       /sys/devices/platform/i8042/
            declare -a INPUT_DEVICES=(/sys/class/input/*)
            I8042_INPUT_DEVICE_REGEX='^/sys/devices/platform/i8042/serio[[:digit:]]+/input/input[[:digit:]]+$'
            for INPUT_DEVICE_PATH in "${INPUT_DEVICES[@]}"; do
                INPUT_DEVICE_REALPATH="$(realpath "${INPUT_DEVICE_PATH}")"
                if [[ ${INPUT_DEVICE_REALPATH} =~ ${I8042_INPUT_DEVICE_REGEX} ]]; then
                    chmod o+rx "${INPUT_DEVICE_REALPATH}"/                     \
                               "${INPUT_DEVICE_REALPATH}"/event+([[:digit:]])/
                    chmod o+r "${INPUT_DEVICE_REALPATH}"/uevent                     \
                              "${INPUT_DEVICE_REALPATH}"/event+([[:digit:]])/uevent

                    # Ensure the DAC permissions for any directories preceding that of the current INPUT_DEVICE are
                    # |     appropriately relaxed.
                    PRECEDING_DIRECTORIES="$(echo "${INPUT_DEVICE_REALPATH}" | grep -o '/' | wc -l)"
                    while [[ ${PRECEDING_DIRECTORIES} -gt 5 ]]; do
                        I8042_DEVICE_PRECEDING_DIR="$(echo "${INPUT_DEVICE_REALPATH}" | cut --delimiter='/' --fields=1-${PRECEDING_DIRECTORIES})"
                        chmod o+rx "${I8042_DEVICE_PRECEDING_DIR}"/
                        ((--PRECEDING_DIRECTORIES))
                    done
                fi
            done
        else
            echo "WARN: Hardening exceptions for a laptops built-in input device/s are enabled but no such devices" \
                 "were able to be located."
        fi

        echo -e "SUCCESS: Hardening exceptions for using a laptop's built-in input device/s with Wayland or X11 were" \
                "applied successfully.\n"
    fi
}

hardening_exceptions_for_usb_input_on_wayland_or_x11() {
    # If enabled configure hardening exceptions allowing non-whitelist group members access to USB input device/s when
    # |     using Wayland or X11:
    if [[ ${EXCEPTIONS_WAYLAND_OR_X11_INPUT_USB} = 'true' ]]; then
        echo "INFO: Attempting to configure hardening exceptions for using USB input device/s with Wayland or X11..."

        chmod o+rx /sys/                    \
                   /sys/bus/                \
                   /sys/bus/pci/            \
                   /sys/bus/pci/devices/    \
                   /sys/class/              \
                   /sys/class/input/        \
                   /sys/dev/                \
                   /sys/dev/char/           \
                   /sys/devices/            \
                   /sys/devices/pci0000:00/

        declare -a INPUT_DEVICES=(/sys/class/input/*)
        USB_INPUT_DEVICE_REGEX='^/sys/devices/pci0000:00/[[:digit:]]+:[[:alnum:]]+:[[:alnum:]]+.[[:digit:]]+/usb[[:digit:]]/.*/input/input[[:digit:]]+$'
        for INPUT_DEVICE_PATH in "${INPUT_DEVICES[@]}"; do
            INPUT_DEVICE_REALPATH="$(realpath "${INPUT_DEVICE_PATH}")"
            if [[ ${INPUT_DEVICE_REALPATH} =~ ${USB_INPUT_DEVICE_REGEX} ]]; then
                chmod o+rx "${INPUT_DEVICE_REALPATH}"/                     \
                           "${INPUT_DEVICE_REALPATH}"/event+([[:digit:]])/
                chmod o+r "${INPUT_DEVICE_REALPATH}"/uevent                     \
                          "${INPUT_DEVICE_REALPATH}"/event+([[:digit:]])/uevent

                # Ensure the DAC permissions for any directories preceding that of the current INPUT_DEVICE are
                # |     appropriately relaxed.
                PRECEDING_DIRECTORIES="$(echo "${INPUT_DEVICE_REALPATH}" | grep -o '/' | wc -l)"
                while [[ ${PRECEDING_DIRECTORIES} -gt 4 ]]; do
                    USB_DEVICE_PRECEDING_DIR="$(echo "${INPUT_DEVICE_REALPATH}" | cut --delimiter='/' --fields=1-${PRECEDING_DIRECTORIES})"
                    chmod o+rx "${USB_DEVICE_PRECEDING_DIR}"/
                    ((--PRECEDING_DIRECTORIES))
                done
            fi
        done

        echo -e "SUCCESS: Hardening exceptions for using USB input device/s with Wayland or X11 were applied" \
                "successfully.\n"
    fi
}


hardening_exceptions_for_virtio_input_on_wayland_or_x11() {
    # If enabled configure hardening exceptions allowing non-whitelist group members access to VIRTIO input device/s
    # |     when using Wayland or X11:
    if [[ ${EXCEPTIONS_WAYLAND_OR_X11_INPUT_VIRTIO} = 'true' ]]; then
        echo "INFO: Attempting to configure hardening exceptions for using VIRTIO input device/s with Wayland or X11..."

        chmod o+rx /sys/                    \
                   /sys/bus/                \
                   /sys/bus/pci/            \
                   /sys/bus/pci/devices/    \
                   /sys/class/              \
                   /sys/class/input/        \
                   /sys/dev/                \
                   /sys/dev/char/           \
                   /sys/devices/            \
                   /sys/devices/pci0000:00/

        declare -a INPUT_DEVICES=(/sys/class/input/*)
        VIRTIO_INPUT_DEVICE_REGEX='^/sys/devices/pci0000:00/[[:digit:]]+:[[:alnum:]]+:[[:alnum:]]+.[[:digit:]]+/[[:digit:]]+:[[:digit:]]+:[[:digit:]]+.[[:digit:]]+/virtio[[:digit:]]+/input/input[[[:digit:]]+$'
        for INPUT_DEVICE_PATH in "${INPUT_DEVICES[@]}"; do
            INPUT_DEVICE_REALPATH="$(realpath "${INPUT_DEVICE_PATH}")"
            if [[ ${INPUT_DEVICE_REALPATH} =~ ${VIRTIO_INPUT_DEVICE_REGEX} ]]; then
                chmod o+rx "${INPUT_DEVICE_REALPATH}"/                     \
                           "${INPUT_DEVICE_REALPATH}"/event+([[:digit:]])/
                chmod o+r "${INPUT_DEVICE_REALPATH}"/uevent                     \
                          "${INPUT_DEVICE_REALPATH}"/event+([[:digit:]])/uevent

                # Ensure the DAC permissions for any directories preceding that of the current INPUT_DEVICE are
                # |     appropriately relaxed.
                PRECEDING_DIRECTORIES="$(echo "${INPUT_DEVICE_REALPATH}" | grep -o '/' | wc -l)"
                while [[ ${PRECEDING_DIRECTORIES} -gt 4 ]]; do
                    VIRTIO_DEVICE_PRECEDING_DIR="$(echo "${INPUT_DEVICE_REALPATH}" | cut --delimiter='/' --fields=1-${PRECEDING_DIRECTORIES})"
                    chmod o+rx "${VIRTIO_DEVICE_PRECEDING_DIR}"/
                    ((--PRECEDING_DIRECTORIES))
                done
            fi
        done

        echo -e "SUCCESS: Hardening exceptions for using VIRTIO input device/s with Wayland or X11 were applied" \
                "successfully.\n"
    fi
}


hardening_exceptions_for_kloak_input_on_wayland_or_x11() {
    # If enabled configure hardening exceptions allowing non-whitelist group members access to the KLOAK input device
    # |     when using Wayland or X11:
    if [[ ${EXCEPTIONS_WAYLAND_OR_X11_INPUT_KLOAK} = 'true' ]]; then
        echo "INFO: Attempting to configure hardening exceptions for using KLOAK with Wayland or X11..."

        if KLOAK_DEVICE="$(grep --no-messages --files-with-matches '^kloak$' /sys/devices/virtual/input+([[:digit:]])/name)"; then
            chmod o+rx /sys/                       \
                       /sys/class/                 \
                       /sys/class/input/           \
                       /sys/dev/                   \
                       /sys/dev/char/              \
                       /sys/devices/               \
                       /sys/devices/virtual        \
                       /sys/devices/virtual/input/

            KLOAK_DEVICE_PATH="$(dirname "${KLOAK_DEVICE}")"
            chmod o+rx "${KLOAK_DEVICE_PATH}"/                     \
                       "${KLOAK_DEVICE_PATH}"/event+([[:digit:]])/
            chmod o+r "${KLOAK_DEVICE_PATH}"/uevent                      \
                       "${KLOAK_DEVICE_PATH}"/event+([[:digit:]])/uevent
        else
            echo "WARN: Hardening exceptions for KLOAK are enabled but we were unable to locate KLOAK's input" \
                 "device. Ensure the KLOAK service is started before this script is run."
        fi

        echo -e "SUCCESS: Hardening exceptions for using KLOAK with Wayland or X11 were applied successfully.\n"
    fi
}


hardening_exceptions_for_gpu_utilization_by_firefox() {
    # If enabled configure hardening exceptions allowing non-whitelist group members to utilize GPU device/s with
    # |     Firefox:
    if [[ ${EXCEPTIONS_WAYLAND_OR_X11_FIREFOX_GPU} = 'true' ]]; then
        echo "INFO: Attempting to configure hardening exceptions for Firefox GPU utilization..."

        # Unfortunately due to a questionable GPU detection mechanism far more exceptions than necessary are required...
        chmod o+rx /sys/devices/pci0000:00/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/     \
                   /sys/devices/pci0000:00/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/
        chmod o+r /sys/devices/pci0000:00/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/{class,device,vendor}     \
                  /sys/devices/pci0000:00/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/+([[:digit:]]):+([[:alnum:]]):+([[:alnum:]]).+([[:digit:]])/{class,device,vendor}

        echo -e "SUCCESS: Hardening exceptions for Firefox GPU utilization were applied successfully.\n"
    fi
}


hardening_exceptions_for_libvirt_qemu_user_session_vms() {
    # If enabled configure hardening exceptions allowing non-whitelist group members access to resources required for
    # |     running libvirt "QEMU://user-session" VMs:
    if [[ ${EXCEPTIONS_LIBVIRT_QEMU_USER_SESSION} = 'true' ]]; then
        echo "INFO: Attempting to configure hardening exceptions for libvirt \"QEMU://user-session\" VMs..."

        chmod o+rx /sys/                    \
                   /sys/devices/            \
                   /sys/devices/system/     \
                   /sys/devices/system/cpu/
        chmod o+r /sys/devices/system/cpu/online  \
                  /sys/devices/system/cpu/present

        echo -e "SUCCESS: Hardening exceptions for libvirt \"QEMU://user-session\" VMs were applied successfully.\n"
    fi
}


# Implement any enabled exceptions to the base hardening configuration:
implement_base_hardening_exceptions() {
    echo "INFO: Attempting to apply any enabled exceptions to the base hardening configuration..."

    hardening_exceptions_for_selinux
    hardening_exceptions_for_hardware_sensors
    hardening_exceptions_for_video_devices_on_wayland_or_x11
    hardening_exceptions_for_laptop_builtin_input_on_wayland_or_x11
    hardening_exceptions_for_usb_input_on_wayland_or_x11
    hardening_exceptions_for_virtio_input_on_wayland_or_x11
    hardening_exceptions_for_kloak_input_on_wayland_or_x11
    hardening_exceptions_for_gpu_utilization_by_firefox
    hardening_exceptions_for_libvirt_qemu_user_session_vms

    echo "SUCCESS: Any enabled exceptions to the base hardening configuration were applied successfully."
}


implement_base_hardening
implement_base_hardening_exceptions
