# Overview
Like all projects, there are going to be inevitable hiccups along the way. Some of the errors can be self-corrected, and we want to ensure that issues that have been debugged are recorded appropriately so that no one has to reinvent the wheel. 

## Installer Issues
A variety of issues can crop up with the installer such as:
- Spotty connections can halt the download
- Staging can change over time - particularly with Whonix / Kicksecure
- Changed dependencies 
- Base machine's clock is skewed
- A file reference could be missing or misplaced

In short, a proper connection via Ethernet, installation over clearnet rather than TOR, and pre-determining an accurate clock time can save you a lot of headache. 

---

## Baseline PlagueOS issues

### Libvirt / virt-manager
#### Virtual Machines (VM) not starting
1. Is this your first boot?
 - If so, open `virt-manager`, select `Connection Details` -> `Virtual Networks`
 - Select each virtual network individually, ensure `On Boot` is checked, and click the start / play button at the bottom to ensure the network is turned on.

#### Virtual Machines (VM) not displaying
1. Is `virt-manager` not displaying your VMs?
    - From a privileged user (i.e. admin or root), run the following commands:
      - `sv restart virtlogd`
      - `sv restart virtlockd`
      - `sv restart libvirtd`
    - Reopen `virt-manager` and the VMs should appear

### Kernel level
Kernel security, particularly with PlagueOS, is generally untrodden ground. Not many seek out this esoteric level of security. Many are initimidated by the kernel layer, and documentation surrounding this topic is an utter mess. 

#### Adding built-in modules
The Linux kernel is primarily known for this statement - "It just works."
This is problematic for the security enthusiast who does not want to support legacy modules from the 90's. 

The modules are signed by an ephemeral signing key during compilation. If the kernel is missing a necessary driver for your operations, there are limited way to correct this. 
1. You wait for our team to get around to pushing an update to the kernel, and you run the `plague-update` script from a privileged user
2. You take action into your own hands and self-compile. Before you call us maniacs for suggesting this, we have built a script for self-compiling. The Plague Kernel [self_compilation.sh](https://0xacab.org/optout/plague-kernel/self_compilation.sh) script does all of the legwork. 
  - A quick note regarding this, we would strongly advise running with the default Plague kernel configuration rather than the fingerprinting hardware option to start. You can optionally choose to open the kernel configuration menu, and then select the module(s) with you are missing. This search can be done by running `/` in the UI that appears. 
  
  We do not advise trying to fingerprint your hardware while trying to make things work. This could vastly overcomplicate the problem, and could even result in an unbootable kernel. We wish that we could provide full assurances that we could guarantee a working, self-compiled kernel on a variety of different hardware and software, however this sadly is not a reality. 

#### Adding external modules
This is indeed a limitation. The Plague Kernel & self-compile script are designed to sign the present modules and then burn the ephemeral signing key. We also have no desire or plans to bake in third party modules during our upstream build process. 

If you need external driver(s) that are not supported in the upstream Linux kernel, the only solutions you have are as follows:
- Install and run with the LTS kernel and forfeit the mitigations of the minimalist / hardened kernel
- Self-compile and bake in 3rd party module
