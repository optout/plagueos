# About PlagueOS
PlagueOS is not associated with the TOR project or Whonix.
This is not developed by an organization; this is a passion project created and maintained by a few Linux enthusiasts.
This OS is not resistant to all forms of exploitation; there are shortcomings inherent to the modern desktop environment that we are attempting to mitigate.
The OS is designed to run KVM as a minimalist hypervisor with security-centric virtual machines (guests) from `virt-manager`.
Whonix and Kicksecure can be imported and validated during the installation script. Any other guests will be a manual endeavor.

# Proper Usage
There are two users by default: `admin` and `user`.
All updates, upgrades, and package installations (perhaps KeepassXC or ZuluCrypt) must be installed by the `admin` account.
All daily activities are to be conducted by the unprivileged `user` account. The `user` account has permissions to access `virt-manager` and the related guests.

## File and Folder Permissions
By default, PlagueOS has a strong permission set.
This means that newly created files will be unaccessible by the `user` account, and potentially by other applications.
The permissions of files and folders can be checked by navigating to their directory and typing `ls -la` inside the terminal/console.
The output you get means: `r` = read, `w` = write, `x` = execute.
To change the permissions of either files or folders, type the `chmod` command inside the terminal, as `admin`: --> `chmod <value> <file/folder>`
There are plenty of sites that can help you to understand how the `chmod` command works. Search `chmod calculator` with your preferred search engine.

## Mounting USB Devices
`USBCTL` is present on the installation to prevent auto-mounting of USB Drives, as they are a commonly known attack vector. To allow a USB device to be mounted, you must run `usbctl unprotect`, connect the device, and run `usbctl protect` from the `admin` account. 

## Traffic Routing
By default, the packages `openvpn` and `wireguard` are not present on the host. If a VPN is desired, these packages or custom wrappers from the provider must be installed.
`TOR` can be configured during install to be leveraged for all package updates and installations. NOTE: Not all traffic is routed through `TOR`, and software-based routing solutions are not impervious to leakage.

# Limitations
This OS cannot mitigate the actions which are caused by user error. If you are acting outside of the bounds of [proper usage](#proper-usage), this is outside the scope of our mitigations.

# Baby Steps
Do note that this project is still in its infancy, and bugs across different classes of hardware are likely to occur. We will help with bugs the best that we can, however we cannot investigate an error that we cannot reproduce or visualize. We appreciate any assistance with bug reporting.

# Development
All contributions to the PlagueOS project are welcome. This could include actively testing exploitation mechanisms against the OS from VM escapes to privilege escalation from the user account, pushing or providing steps to remove a bug, steps to include to implement one of the listed feature requests, or suggesting/porting over features that can increase the overall security of PlagueOS. See more detail into our DevOps process [here](https://0xacab.org/optout/plagueOS/wiki/Development).
