# General Rules
1. All commits must be signed with GPG [key](#gpg-signature)
2. Branch Creation targets specific issue
    - Specific branch creation from issue -> dev branch -> master
3. PR requests must be issued for auditing purposes

## Add SSH key to profile
Generate SSH keypair:

`ssh-keygen -t ed25519 -C "your_email@example.com"`

Start ssh-agent in background:
`eval "$(ssh-agent -s)"`

Add SSH private key to ssh-agent:
`ssh-add ~/.ssh/id_ed25519`

`git config --global user.email "[my email]"`

`git config --global user.name "[my username]"`

`touch /home/[my username]/.ssh/config`

Add public key `id_ed25519.pub` to git profile

## GPG Signature
Generate a key with `gpg --full-generate-key`

First, add GPG key via Web GUI

To find the key identifier:
```
From the list of GPG keys, copy the long form of the GPG key ID you'd like to use. In this example, the GPG key ID is **3AA5C34371567BD2**:

$ gpg --list-secret-keys --keyid-format=long

------------------------------------
sec   4096R/3AA5C34371567BD2 2016-03-10 [expires: 2017-03-10]
uid                          Hubot 
ssb   4096R/42B317FD4BA89E7A 2016-03-10
```
Secondly, set signingkey via terminal:
`git config --global user.signingkey 3AA5C34371567BD2`

`git config --global commit.gpgsign true`

Add key to bash profile:
```
if [ -r ~/.bash_profile ]; then echo 'export GPG_TTY=$(tty)' >> ~/.bash_profile; \
  else echo 'export GPG_TTY=$(tty)' >> ~/.profile; fi
```
  
  ### To sign commits
 
 `git commit -S -m "your commit message"` (Keep comments short & direct)

 `git push`

## Frequently Used Git commands
### Pull Repository down locally
`git clone git@0xacab.org:WhichDoc/plagueOS.git`

### Update branch w/ remote dev
`git remote add upstream https://0xacab.org/optout/plagueos`

`git fetch upstream`

#### Merge in upstream changes

`git checkout dev`

`git merge upstream/dev`

### Revert HEAD

`git reset --hard <SHA1 hash>`

`git push --force origin HEAD`
