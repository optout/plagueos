- Why [Void Linux](https://voidlinux.org/)?
    - [MUSL](https://musl.libc.org/) codebase
        - The Void MUSL build was selected over a codebase like glibc due to its minimalist structure. Due to its relatively small attack surface, significantly less vulnerabilities have been identified in MUSL vs. glibc. While counting disclosed vulnerabilities is not in itself an accurate measurement, it could stand to identify overarching issues in the codebase. See [here](https://www.dustri.org/b/security-features-of-musl.html) for a list (incomplete) of MUSL hardening features.
    - Selection of Runit over SystemD
        - [Runit](https://en.wikipedia.org/wiki/Runit) is a service manager that has a relatively simple codebase, while SystemD has a monolithic codebase. There have been many conspiracies in the past about implanted backdoors in the SystemD codebase that are unsubstantiated. These claims were likely overblown due to the [overreach](https://ewontfix.com/14/) that surpassed necessary function and their desire to be enforced across all distributions. While following the concept of minimalism, runit was the clear path forward.
- Hardened Memory Allocator system-wide LD_PRELOAD
    - This is ported over from the [GrapheneOS project](https://github.com/GrapheneOS/hardened_malloc) and complements the MUSL codebase well. See [here](https://github.com/GrapheneOS/hardened_malloc#security-properties) for further elaboration on the security properties of hardened_malloc.
- Hardened Kernel with patchsets and trimming
    - The Linux kernel is a monolithic mess that is vulnerable to classes of exploitation. Patchsets and gutted kernel modules stand to limit the attack surface, and thus kill off classes of exploitation. Further details can be found [here](https://0xacab.org/optout/plague-kernel).
- Kernel Boot Parameters
    - The GRUB_CMDLINE_LINUX_DEFAULT line in `/etc/default/grub` contains parameters used to mitigate a variety of software and hardware related vulnerabilities. These parameters stand to complement the already hardened [plague-kernel](https://0xacab.org/optout/plague-kernel) configuration. While there is no one-size fits all for every mitigation across all hardware, we are attempting to perform a balancing act between hardening and system performance. See [linux-hardened](https://raw.githubusercontent.com/anthraxx/linux-hardened/5.10/Documentation/admin-guide/kernel-parameters.txt) kernel documentation and [hardware vulnerability](https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/) related parameters.
      - `intel_iommu` & `amd_iommu` - Pass parameters to the Intel/AMD IOMMU driver in the system.
      - `iommu=force` - Enforces the use of IOMMU.
        - `iommu.strict=1` was previously used, however due to performance issues this has since been removed.
        - `amd_iommu=force_isolation` is in a similar position as the strict option.
      - `slub_debug=FZ` - Creates guard zones around objects may poison objects when not in use. FZ ensure that sanity checks are on (enables SLAB_DEBUG_CONSISTENCY_CHECKS) along with red zoning.
      - `pti=on` - Enables Page Table Isolation of user and kernel address spaces.
      - `spectre_v2=on` - Unconditionally enables the mitigation of Spectre variant (indirect branch speculation) vulnerability.
      - `spec_store_bypass_disable=on` - Certain CPUs are vulnerable to an exploit against a common industry wide performance optimization known as "Speculative Store Bypass" in which recent stores to the same memory location may not be observed by later loads during speculative execution. The idea is that such stores are unlikely and that they can be detected prior to instruction retirement at the end of a particular speculation execution window. In vulnerable processors, the speculatively forwarded store can be used in a cache side channel attack, for example to read memory to which the attacker does not directly have access (e.g. inside sandboxed code). This parameter unconditionally disables SSB.
      - `tsx=off` - Disables Transactional Synchronization Extensions (TSX) feature for Intel processors. TSXs are vulnerable to an exploit against CPU internal buffers which can forward information to a disclosure gadget under certain conditions. In vulnerable processors, the speculatively forwarded data can be used in a cache side channel attack, to access data to which the attacker does not have direct access.
      - `tsx_async_abort=full` - Enable TAA mitigation on vulnerable CPUs if TSX is enabled.
      - `l1tf=flush` - Leaves SMT enabled and enables the default hypervisor mitigation, i.e. conditional L1D flushing.
      - `mds=full` - Enables Micro-architectural Data Sampling (MDS) mitigation on vulnerable CPUs
      - `mitigations=auto,nosmt` - Disable symmetric multithreading (SMT). Do note that this parameter can lead to performance issues, but it will only disable SMT when the CPU is known to be vulnerable. Unimpacted CPU's will still run with the full amount of system cores. 
- FDE Custom LUKS Encryption (AES256XTS+Argon2id KDF)
    - Cipher primitives
        - AES256-XTS
            - AES256 has been chosen as the encryption cipher as it has a good balance of security and speed. While some may argue it is less cryptographically secure than Anubis or Serpent, the difference is marginal and the cipher makes it up in speed especially if the user has a CPU containing a hardware accelerated AES instruction set (~4x faster).
            - The XTS mode of operation has been selected over CBC as it is considered less malleable than CBC, however it should be noted that neither cipher is meant for integrity protection, but XTS is excellent at confidentiality as recommended by [NIST SP800-38E](https://csrc.nist.gov/publications/detail/sp/800-38e/final). [BTRFS](https://www.kernel.org/doc/html/latest/filesystems/btrfs.html) filesystem alleviates the concern by providing checksums for all data and metadata on the disk.
        - Argon2id
            - Argon2id was selected as the key derivation function cipher as it currently holds the best combined [side-channel](https://en.wikipedia.org/wiki/Side-channel_attack) resistance, parallelism resistance, and [time memory trade-off attack](https://en.wikipedia.org/wiki/Time/Memory/Data_Tradeoff_attack) resistance.
      - The chosen cryptographic cipher and key derivation (KDF) can be altered by editing the following lines prior to install in the [env.cfg](https://0xacab.org/optout/plagueos/-/blob/master/installerScripts/env.cfg) file:

            - CIPHER="aes-xts-plain64" -> "serpent"
            - KEY_SIZE="512" -> "1024"
            - PBKDF="argon2id" -> "pbkdf2"

    - WIP: LUKS Keyfile (optional)
        - The keyfile provides more entropy to unlock than a standard passphrase.
    - WIP: Separate Keyfile
        - Using a keyfile to encrypt your LUKS partition and separating it from the drive makes it so that a threat actor would require your drive containing the LUKS partition and the USB containing the keyfile to unlock your LUKS partition. Storing a keyfile on a USB key provides an easy way to destroy the keyfile (leaving the drive forever encrypted), and through the "losing" your keyfile you could prevent a [rubberhose](https://en.wikipedia.org/wiki/Rubber-hose_cryptanalysis) attack.
    - WIP: Encrypted Keyfile
        - Extending the security by encrypting the keyfile (through GPG, lukskey, or OpenSSL) provides a method of multi-factor authentication as the threat actor would now require access to your drive, your USB key, and the passphrase to unlock your keyfile in order to unlock your LUKS partition.
    - WIP: Separate Boot (optional)
        - Removing the /boot from the same drive containing the LUKS partition would prevent an attacker from loading their custom GRUB parameters or kernel/initramfs. It provides less information to the threat actor about what you're intending to boot into. It also provides some plausible deniability as your BIOS would not find the EFI entry to boot into your Void installation and would select the next OS if one is found (in a dual-boot system).
    - WIP: Detached LUKS header (optional)
        - Removing the header from the same drive containing the LUKS partition provides cryptographic deniability as a threat actor would not be able to distinguish whether the drive contains encrypted data or an empty drive filled with random bytes. It also provides less information to the threat actor about how your LUKS partition is configured and what ciphers you are using for them to attempt decryption.
    - WIP: Nuke LUKS keys by using a special passphrase (See reference [here](https://gitlab.com/kalilinux/packages/cryptsetup-nuke-keys))
        - The Cryptsetup package with the LUKS nuke feature is planned due to its anti-forensic nature. In short, this allows the user to configure a secondary passphrase that could be leveraged when under duress to nuke the LUKS headers, rendering the host's data permanently encrypted. (Note: The headers can be restored with a backup)
- Blacklisted kernel modules, file systems, & network protocols
    - Sysctl parameters and modprobe blacklist are passed to configure various security features & disable unnecessary or dangerous features to reduce attack surface. Our sysctl parameters are listed [here](https://0xacab.org/optout/plagueOS/src/branch/master/configs/etc/sysctl.d) and the blacklist is listed [here](https://0xacab.org/optout/plagueOS/src/branch/master/configs/etc/modprobe.d/blacklist.conf). Do note that many of the legacy modules/protocols highlighted in the modprobe blacklist are already mitigated at the kernel level and is largely redundant.
- IPTables Packet Filtering
    - IPTables is a local packet filter that is configured on plagueOS to deny all inbound and forwarding traffic.
- Introduced a rewrite of Whonix's hide-hardware-info script that has a working implementation.
- Increased Entropy with use of `haveged` and `jitterentropy`
    - All strong cryptographic implementations rely on entropy or randomness. To increase entropy on GNU/Linux systems, the packages `haveged` and `jitterentropy` can be used along with the boot parameter `random.trust_cpu=off` in the `/etc/default/grub` file. See [Madaidan's Linux hardening guide](https://madaidans-insecurities.github.io/guides/linux-hardening.html#entropy) and [Into the Crypt: The Art of Anti-Forensics](https://0xacab.org/optout/into-the-crypt#cryptography) for more details on system entropy.
- Increased password hashing rounds
- Full Wayland Environment options
    - Wayland environments were prioritized due to their implementation of windows isolation, which Xorg lacks. Xorg windows with a nested X11 server and a sandboxing tool such as `bwrap` could accomplish this, however Wayland provides the better model by default. We currently leverage `gnome-core` as the desktop environment to provide a minimalist, easy-to-use interface.
    - Window Manager options are still under consideration, however, few truly abide by the Wayland security model as observed with Sway WM (which we previously supported).
- Hide process identifiers
    - By default, all users can view snoop on process identifiers, which could be leveraged by an attacker to spy on processes. We mount `/proc` with `hidepid=2,gid=proc` mount options.
- Permission hardening
- UMASK 0077 to system-wide default
    - `umask` sets the default file permissions for newly created files. This makes new files unreadable by anyone other than the owner.
- Secure fstab configuration
    - See the set fstab [configuration](https://0xacab.org/optout/plagueOS/src/branch/master/configs/etc/fstab) detailing restrictions set to mount points. `nodev`, `nosuid`, and `noexec` are a few of the set flags that were applied to specific mount points.
- Locked `root` account, `admin` account for elevated privileges
- Use of doas over sudo
    - The package [doas](https://wiki.archlinux.org/title/Doas) is a simple alternative to `sudo` which forces a password entry for every command issued, unlike sudo which has a specified time in a shell where it will retain full administrative function.
- Generic Machine ID
    - The machine ID is altered to a generic identifier to reduce host fingerprinting.
- Randomized MAC address for Network Interface Controllers (NIC)
    - The implementation for randomizing the MAC address includes the use of `bwrap` in the [plague-macchanger](https://0xacab.org/optout/plagueOS/src/branch/master/bin/plague-macchanger) service. This alters the MAC upon every boot rather than every new connection. We mimic vendor identifiers in the first bits and randomize the last half. Having a completely random MAC address stands to make your device an anomaly rather than blending.
- [USBCTL](https://github.com/anthraxx/usbctl/) Implementation
    - This prevents arbitrary mounting of USBs that could be malicious. This is a reduction on physical attack surface. On PlagueOS, you must run `usbctl unprotect` to allow USB connections from the privileged `admin` account. The USBCTL is a strong mitigation, given that it operates at the kernel level. It is sufficient to prevent automounting. 
- Encrypted DNS with [DNSCrypt](https://github.com/DNSCrypt/dnscrypt-proxy) by default.
- Ported in `plague-time-sync`
    - This is a simple mechanism using curl to set the date variable. This removes the need for syncing to NTP pools and the presence of services such as `ntpd`. The current configuration of Plague executes `plague-time-sync` via `bwrap` sandbox from the runit service `/plague/runit/plague-time-sync` and subsequently terminates the parent process once the date variable has been set. This occurs on a frequency of 6 hours, however the user can execute `plague-time-sync` at any time with the use of `doas` from the privileged `admin` account.
- Added `scurl` (secure-curl) to `/usr/bin/`
    - For all executed curl commands to HTTPS sites, `scurl` should be ran than `curl` to prevent TLS downgrade attacks. The installer has these mitigations in place for every executed `curl` command.
- Hardened SSH configuration (SSH not installed on host by default)
- WIP: Self-Signed Verified Boot
