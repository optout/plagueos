Donations to support current and related works are welcome with Monero (XMR) in the spirit of cryptography and anti-forensics.

Monero (XMR): `47w2kanKMnzFkRGnSvbYjjPYac9TAsAm2GzmPBprdqM41zVXHSgkkSmVJMrY6o1qoYLdVJabcBupnJbABMxu4ejrMArAEue`

Also listed in [Into the Crypt: The Art of Anti-Forensics](https://0xacab.org/optout/into-the-crypt#donations)
