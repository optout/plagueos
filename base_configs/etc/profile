# /etc/profile

# Set our umask and explicitly disable core dumps
umask 0077
ulimit -c 0

# Append "$1" to $PATH when not already in.
# This function API is accessible to scripts in /etc/profile.d
append_path () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="${PATH:+$PATH:}$1"
    esac
}

# Append our default paths
append_path '/usr/local/sbin'
append_path '/usr/local/bin'
append_path '/usr/bin'

# Force PATH to be environment
export PATH

# Our environment variable Tweaks
#
# Sets the nano text editor to be the default for sudoedit
export EDITOR=nano
# Remove the Api key (Used for logging into a Google account) is missing warning and other tweaks
export GOOGLE_API_KEY="no"
export GOOGLE_DEFAULT_CLIENT_SECRET="no"
export GOOGLE_DEFAULT_CLIENT_ID="no"

# Load profiles from /etc/profile.d
if test -d /etc/profile.d/; then
    for profile in /etc/profile.d/*.sh; do
        test -r "$profile" && . "$profile"
    done
    unset profile
fi

# Unload our profile API functions
unset -f append_path

# Source global bash config, when interactive but not posix or sh mode
if test "$BASH" &&\
   test "$PS1" &&\
   test -z "$POSIXLY_CORRECT" &&\
   test "${0#-}" != sh &&\
   test -r /etc/bash/bashrc
then
    . /etc/bash/bashrc
fi

# Termcap is outdated, old, and crusty, kill it.
unset TERMCAP

# Man is much better than us at figuring this out
unset MANPATH
