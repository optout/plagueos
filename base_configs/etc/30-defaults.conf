# Miscellaneous settings:
## Cause the hide-hardware-info script to run in a more verbose "debug" mode:
DEBUG_MODE="false"


# Whitelist group settings:
## Enable utilization of the /proc/cpuinfo whitelist group:
WHITELIST_GROUP_CPUINFO="true"
## Name of the group used for our /proc/cpuinfo whitelisting:
CPUINFO_GROUP_NAME="cpuinfo"

## Enable utilization of the /sys whitelist group:
WHITELIST_GROUP_SYSFS="true"
## Name of the group used for our /sys whitelisting:
SYSFS_GROUP_NAME="sysfs"


# Hardening exception settings:
## Enable hardening exceptions required to support SELinux:
EXCEPTIONS_SELINUX="false"

## Enable exceptions required to support non-whitelist group members accessing resources pertaining to specifc hardware
## |    sensor data, CPU temperature, fan speed, etc:
EXCEPTIONS_HARDWARE_SENSORS="false"

## Enable exceptions required to support non-whitelist group members utilizing GPU devices with software such as Wayland
## |    or X11:
EXCEPTIONS_WAYLAND_OR_X11_GPU="true"

## Enable exceptions required to support non-whitelist group members utilizing a laptop's built-in input devices with
## |    Wayland or X11:
EXCEPTIONS_WAYLAND_OR_X11_INPUT_LAPTOP_BUILTIN="true"

## Enable exceptions required to support non-whitelist group members utilizing USB input devices with Wayland or X11:
EXCEPTIONS_WAYLAND_OR_X11_INPUT_USB="true"

## Enable exceptions required to support non-whitelist group members utilizing VIRTIO input devices with Wayland or X11:
EXCEPTIONS_WAYLAND_OR_X11_INPUT_VIRTIO="false"

## Enable exceptions required to support non-whitelist group members utilizing virtual input devices provided by the
## |    "KLOAK" program with Wayland or X11:
EXCEPTIONS_WAYLAND_OR_X11_INPUT_KLOAK="false"

## Enable exceptions required to support non-whitelist group members utilizing GPUs with Firefox, these exceptions are
## |    unreasonably excessive due to the questionable device detection mechanism utilized by Firefox:
EXCEPTIONS_WAYLAND_OR_X11_FIREFOX_GPU="false"

## Enable exceptions required to support non-whitelist group members running libvirt "QEMU://user-session" VMs:
EXCEPTIONS_LIBVIRT_QEMU_USER_SESSION="true"
