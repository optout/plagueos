# Listing a module here prevents the hotplug scripts from loading it.
# Usually that'd be so that some other driver will bind it instead,
# no matter which driver happens to get probed first. Sometimes user
# mode tools can also control driver binding.

# watchdog drivers
blacklist i8xx_tco
blacklist acquirewdt
blacklist advantechwdt
blacklist alim1535_wdt
blacklist alim7101_wdt
blacklist cpu5wdt
blacklist eurotechwdt
blacklist hpwdt
blacklist i6300esb
blacklist iTCO_vendor_support
blacklist iTCO_wdt
blacklist ib700wdt
blacklist ibmasr
blacklist it8712f_wdt
blacklist machzwd
blacklist mixcomwd
blacklist pc87413_wdt
blacklist pcwd
blacklist pcwd_pci
blacklist pcwd_usb
blacklist sbc60xxwdt
blacklist sbc7240_wdt
blacklist sbc8360
blacklist sbc_epx_c3
blacklist sc1200wdt
blacklist sc520_wdt
blacklist scx200_wdt
blacklist smsc37b787_wdt
blacklist softdog
blacklist w83627hf_wdt
blacklist w83697hf_wdt
blacklist w83877f_wdt
blacklist w83977f_wdt
blacklist wafer5823wdt
blacklist wdt
blacklist wdt_pci
#
install i8xx_tco /bin/false
install acquirewdt /bin/false
install advantechwdt /bin/false
install alim1535_wdt /bin/false
install alim7101_wdt /bin/false
install cpu5wdt /bin/false
install eurotechwdt /bin/false
install hpwdt /bin/false
install i6300esb /bin/false
install iTCO_vendor_support /bin/false
install iTCO_wdt /bin/false
install ib700wdt /bin/false
install ibmasr /bin/false
install it8712f_wdt /bin/false
install machzwd /bin/false
install mixcomwd /bin/false
install pc87413_wdt /bin/false
install pcwd /bin/false
install pcwd_pci /bin/false
install pcwd_usb /bin/false
install sbc60xxwdt /bin/false
install sbc7240_wdt /bin/false
install sbc8360 /bin/false
install sbc_epx_c3 /bin/false
install sc1200wdt /bin/false
install sc520_wdt /bin/false
install scx200_wdt /bin/false
install smsc37b787_wdt /bin/false
install softdog /bin/false
install w83627hf_wdt /bin/false
install w83697hf_wdt /bin/false
install w83877f_wdt /bin/false
install w83977f_wdt /bin/false
install wafer5823wdt /bin/false
install wdt /bin/false
install wdt_pci /bin/false

# framebuffer drivers
blacklist radeonfb
blacklist cirrusfb
blacklist intelfb
blacklist kyrofb
blacklist hgafb
blacklist nvidiafb
blacklist rivafb
blacklist neofb
blacklist tridentfb
blacklist vga16fb
blacklist arcfb
blacklist aty128fb
blacklist atyfb
blacklist clgenfb
blacklist cyber2000fb
blacklist fbcon-cfb2
blacklist fbcon-cfb4
blacklist fbcon-hga
blacklist fbcon-mfb
blacklist fbcon-vga-planes
blacklist fbgen
blacklist i2c-matroxfb
blacklist i810fb
blacklist matroxfb_DAC1064
blacklist matroxfb_Ti3026
blacklist matroxfb_accel
blacklist matroxfb_base
blacklist matroxfb_crtc2
blacklist matroxfb_g450
blacklist matroxfb_maven
blacklist matroxfb_misc
blacklist pm2fb
blacklist pm3fb
blacklist savagefb
blacklist sisfb
blacklist sstfb
blacklist tdfxfb
#
install radeonfb /bin/false
install cirrusfb /bin/false
install intelfb /bin/false
install kyrofb /bin/false
install hgafb /bin/false
install nvidiafb /bin/false
install rivafb /bin/false
install neofb /bin/false
install tridentfb /bin/false
install vga16fb /bin/false
install arcfb /bin/false
install aty128fb /bin/false
install atyfb /bin/false
install clgenfb /bin/false
install cyber2000fb /bin/false
install fbcon-cfb2 /bin/false
install fbcon-cfb4 /bin/false
install fbcon-hga /bin/false
install fbcon-mfb /bin/false
install fbcon-vga-planes /bin/false
install fbgen /bin/false
install i2c-matroxfb /bin/false
install i810fb /bin/false
install matroxfb_DAC1064 /bin/false
install matroxfb_Ti3026 /bin/false
install matroxfb_accel /bin/false
install matroxfb_base /bin/false
install matroxfb_crtc2 /bin/false
install matroxfb_g450 /bin/false
install matroxfb_maven /bin/false
install matroxfb_misc /bin/false
install pm2fb /bin/false
install pm3fb /bin/false
install savagefb /bin/false
install sisfb /bin/false
install sstfb /bin/false
install tdfxfb /bin/false

# ISDN - see bugs 154799, 159068
blacklist hisax
blacklist hisax_fcpcipnp
#
install hisax /bin/false
install hisax_fcpcipnp /bin/false

# Deprecated BCM4318 driver.
blacklist bcm43xx
#
install bcm43xx /bin/false

# sound drivers
blacklist snd-pcsp
blacklist pcspkr
#
install snd-pcsp /bin/false
install pcspkr /bin/false

# OSS (Open Sound System) modules.  This is the old and deprecated Linux
# sound system.  If you want to use OSS, remove the alsa-driver package
# and cut out (or comment out) this list.  With these modules
# blacklisted ALSA will load by default.
blacklist ac97
blacklist ac97_codec
blacklist aci
blacklist ad1816
blacklist ad1848
blacklist ad1889
blacklist adlib_card
blacklist aedsp16
blacklist ali5455
blacklist awe_wave
blacklist btaudio
blacklist cmpci
blacklist cs4232
blacklist cs4281
blacklist cs46xx
blacklist emu10k1
blacklist es1370
blacklist es1371
blacklist esssolo1
blacklist forte
blacklist gus
blacklist i810_audio
blacklist kahlua
blacklist mad16
blacklist maestro
blacklist maestro3
blacklist maui
blacklist mpu401
blacklist msnd
blacklist msnd_classic
blacklist msnd_pinnacle
blacklist nm256_audio
blacklist opl3
blacklist opl3sa
blacklist opl3sa2
blacklist pas2
blacklist pss
blacklist rme96xx
blacklist sb
blacklist sb_lib
blacklist sgalaxy
blacklist sonicvibes
blacklist sound
blacklist sscape
blacklist trident
blacklist trix
blacklist uart401
blacklist uart6850
blacklist v_midi
blacklist via82cxxx_audio
blacklist wavefront
blacklist ymfpci
#
install ac97 /bin/false
install ac97_codec /bin/false
install aci /bin/false
install ad1816 /bin/false
install ad1848 /bin/false
install ad1889 /bin/false
install adlib_card /bin/false
install aedsp16 /bin/false
install ali5455 /bin/false
install awe_wave /bin/false
install btaudio /bin/false
install cmpci /bin/false
install cs4232 /bin/false
install cs4281 /bin/false
install cs46xx /bin/false
install emu10k1 /bin/false
install es1370 /bin/false
install es1371 /bin/false
install esssolo1 /bin/false
install forte /bin/false
install gus /bin/false
install i810_audio /bin/false
install kahlua /bin/false
install mad16 /bin/false
install maestro /bin/false
install maestro3 /bin/false
install maui /bin/false
install mpu401 /bin/false
install msnd /bin/false
install msnd_classic /bin/false
install msnd_pinnacle /bin/false
install nm256_audio /bin/false
install opl3 /bin/false
install opl3sa /bin/false
install opl3sa2 /bin/false
install pas2 /bin/false
install pss /bin/false
install rme96xx /bin/false
install sb /bin/false
install sb_lib /bin/false
install sgalaxy /bin/false
install sonicvibes /bin/false
install sound /bin/false
install sscape /bin/false
install trident /bin/false
install trix /bin/false
install uart401 /bin/false
install uart6850 /bin/false
install v_midi /bin/false
install via82cxxx_audio /bin/false
install wavefront /bin/false
install ymfpci /bin/false

# Other Vulnerable Modules
blacklist dccp
blacklist sctp
blacklist rds
blacklist tipc
blacklist n-hdlc
blacklist ax25
blacklist netrom
blacklist x25
blacklist rose
blacklist decnet
blacklist econet
blacklist af_802154
blacklist ipx
blacklist appletalk
blacklist psnap
blacklist p8023
blacklist p8022
blacklist can
blacklist atm
blacklist cramfs
blacklist freevxfs
blacklist jffs2
blacklist hfs
blacklist hfsplus
blacklist squashfs
blacklist udf
blacklist cifs
blacklist nfs
blacklist nfsv3
blacklist nfsv4
blacklist gfs2
blacklist vivid
blacklist bluetooth
blacklist btusb
blacklist uvcvideo
blacklist ohci1394
blacklist dv1394
blacklist eth1394
blacklist raw1394
blacklist video1394
blacklist firewire-core
blacklist firewire-net
blacklist firewire-ohci
blacklist firewire-sbp2
blacklist firewire-serial
blacklist sbp_target
blacklist sbp2
blacklist snd-firewire-lib
blacklist snd-firewire-speakers
blacklist firewire
blacklist thunderbolt
blacklist isight_firmware
#
install dccp /bin/false
install sctp /bin/false
install rds /bin/false
install tipc /bin/false
install n-hdlc /bin/false
install ax25 /bin/false
install netrom /bin/false
install x25 /bin/false
install rose /bin/false
install decnet /bin/false
install econet /bin/false
install af_802154 /bin/false
install ipx /bin/false
install appletalk /bin/false
install psnap /bin/false
install p8023 /bin/false
install p8022 /bin/false
install can /bin/false
install atm /bin/false
install cramfs /bin/false
install freevxfs /bin/false
install jffs2 /bin/false
install hfs /bin/false
install hfsplus /bin/false
install squashfs /bin/false
install udf /bin/false
install cifs /bin/false
install nfs /bin/false
install nfsv3 /bin/false
install nfsv4 /bin/false
install gfs2 /bin/false
install vivid /bin/false
install bluetooth /bin/false
install btusb /bin/false
install uvcvideo /bin/false
options ohci1394 phys_dma=0
install ohci1394 /bin/false
install dv1394 /bin/false
install eth1394 /bin/false
install raw1394 /bin/false
install video1394 /bin/false
install snd-firewire-lib /bin/false
install snd-firewire-speakers /bin/false
install sbp2 /bin/false
install sbp_target /bin/false
install firewire-serial /bin/false
install firewire-sbp2 /bin/false
install firewire-ohci /bin/false
install firewire-net /bin/false
install firewire-core /bin/false
install firewire /bin/false
install thunderbolt /bin/false
install isight_firmware /bin/false

# I2C (nvidia specific)
blacklist i2c_nvidia_gpu
install i2c-nvidia-gpu /bin/false

# Intel ME
blacklist mei
blacklist mei-me
#
install mei /bin/false
install mei-me /bin/false
