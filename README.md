# PlagueOS

**Why PlagueOS?** The origin comes from the idea of the project - a GNU/Linux security revival that has been long overdue. As the project description states, this was created to address classes of exploitation that plague standard Linux systems.

We should note that this is a secure baseline of Void Linux rather than a standalone OS. We have no self-hosted architecture, nor do we have our own custom repositories. This can be useful for blending with the Void community at large, along with keeping down costs.

**Is PlagueOS for you?**
If you want a hardened host configuration of GNU/Linux that is designed for operating all processes from Guest virtual machines (VMs), this is designed for that usecase.

For more details regarding the project, please see the following [wiki](https://0xacab.org/optout/plagueos/-/wikis/home).

Feel free to contact us in the PlagueOS matrix [chat](https://matrix.to/#/#plagueos:matrix.org)

# Install Guide

## Prerequisites
1. Internet Connection
    - Ethernet (Recommended)
    - Wireless
        - If desired, issue the following commands:
           - `ip a` (for interface-name)
           - `wpa_passphrase "SSID name" "Wi-Fi password" >> /etc/wpa_supplicant/wpa_supplicant-interface-name.conf`
           - `wpa_supplicant -B -i interface-name -Dnl80211 -c /etc/wpa_supplicant/wpa_supplicant-interface-name.conf`
2. UEFI
3. Recommended minimum 8GB of RAM, 50GB of storage

## Flash USB
- Download the Void Linux musl build from [here](https://voidlinux.org/download/)
> Note: for those not privy to command line, we recommend downloading the XFCE live image.

  - Validate the images with steps described [here](https://docs.voidlinux.org/installation/index.html#verifying-images)
    - Flash the .iso file to a USB Drive
    - For Windows, download `Rufus` or `Balena Etcher` to flash the .iso file
    - For Linux, run the following dd command:
      - Run  `sudo fdisk -l` to show the flashdrive
        - Run `sudo dd status=progress if=void*.iso of=/dev/<USB> conv=fsync`
          - *example:* `/dev/sdb`
        - After the flash has been completed, run `umount /dev/<USB>` to safely unmount the drive

## Base Install
 - Plug in the flash drive and boot from the USB (typically, pressing ESC/F8,F9,F12 allows you to select a boot device upon system startup)
 - Install from Void live image:
    - Start a live session in void
        - Sign in with credentials: `root:voidlinux`
        - `xbps-install -Sy xbps git`
        - `git clone https://0xacab.org/optout/plagueos` or pull down the latest PlagueOS installer [release](https://0xacab.org/optout/plagueos/-/releases)
        - Signature verification (Recommended)
          - Navigate to [KeyOxide](https://keyoxide.org/B8778B5C06CF8D5E69E4D4EB13BA4BD4C14170C0)
            - Click `Verify Signature`
              - Copy & paste the entire release note
              - Click `Verify Signature`
                - If the result does **not** read as `The message was signed by the profile's key.`, please cease the install and notify the PlagueOS matrix chat as soon as possible.
        - Validate the checksum of the file matches the signed message on the release listing:
          - `sha512sum plagueos-*`
          - If the result does **not** match, please cease the install and notify the PlagueOS matrix chat as soon as possible.
        - `tar -xvf plagueos-*`
        - `bash plagueos-*/plague-install -v`

### Install Options
 - During the install, multiple configurations will be displayed. The selections are between the following:
	- `Cli-only` (Minimal installation for advanced users)
	- `Gnome w/ Wayland` (For novice users who desire gutted desktop environment)

## Implementation Goals
- [X] Full System Build
- [X] Hardened Memory Allocator system-wide LD_PRELOAD
- [X] [Hardened Kernel](https://0xacab.org/optout/plague-kernel) w/ patchsets & trimming
- [X] Custom LUKS Encryption (AES256XTS+Argon2id KDF)
- [X] Blacklisted Kernel Modules
- [X] Blacklisted File Systems
- [X] Blacklisted Network Protocols
- [X] IPTables Packet Filtering
- [X] Hardened GRUB Boot Parameters
- [X] Rolled in Whonix's hide-hardware-info script (See [here](https://codeberg.org/SalamanderSecurity/PARSEC/src/branch/main/parsec/bin/hide-hardware-info))
- [X] Increased Entropy with use of `haveged` and `jitterentropy`
- [X] Increased password hashing rounds
- [X] Full Wayland Environment options
- [X] Selection between DE or CLI-only
  - [X] Minimal DE install with `gnome-core`
- [X] Hide Process IDs
- [X] Permission hardening
- [X] UMASK 0077 to system-wide default
- [X] Mountpoint hardening
- [X] Locked `root` account, `admin` account for elevated privileges
- [X] Use of `doas` over `sudo`
- [X] Generic Machine ID
- [X] Randomized MAC address for NIC
- [X] Memory erasure/poisoning
- [X] USBGuard Implementation
- [X] Import & Verification of Kicksecure & Whonix
- [X] Hardened SSH configuration (SSH not installed on host by default)
- [X] Feature: `panic-wipe` included
- [X] All commits to contain PGP signatures

Note: This is not a comprehensive list of hardening, nor does this list intend to provide granular detail. This is simply to highlight some of the security mitigations at a high level.

## Trust Model
It is important to operate on a zero trust model. Since this is an impossible feat, it should at least be known where trust is given and impose restrictions/limitations where feasible.
- Hardware
    - CPU
    - Miscellaneous Integrated Chipsets
- Software
    - Distribution ([Void Linux](https://voidlinux.org/))
    - PlagueOS's installer script
    - [linux-hardened](https://github.com/anthraxx/linux-hardened) kernel
    - [GNOME Core](https://www.gnome.org/) - Optional
    - Graphene's [Hardened Malloc](https://github.com/GrapheneOS/hardened_malloc)
    - Guest OS of Choice (varying levels of security/restrictions controlled by the VM's user)
        - [KickSecure](https://www.whonix.org/wiki/Kicksecure/KVM)
        - [Whonix](https://www.whonix.org/wiki/KVM)
        - Any other OS desired by the user

## Usage
This OS is designed to run as a hypervisor and launch Guest virtual machines (VMs); The OS is designed to be a minimal build and have a restricted userspace. The guests are launched from `virt-manager`.
- Run Virt-Manager either by opening a terminal and running the command `virt-manager` or by opening the application via its GUI.
- During the installation, there is an option to pull down and import [KickSecure](https://www.whonix.org/wiki/Kicksecure/KVM) which is the clearnet baseline of Whonix for day-to-day activities.
- If the torrified version of Kicksecure - [Whonix](https://www.whonix.org/wiki/KVM) - is chosen, you must boot both the `Whonix-Gateway` and `Whonix-Workstation` VMs with `virt-manager`.
	- Note: If the `Whonix-Gateway` is down, the `Whonix-Workstation` will have no internet access.
