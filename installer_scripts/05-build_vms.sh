#!/usr/bin/env bash

###############################
#
#         ### Usage ###
# 1. Install selected VM's onto users host system
#
###############################

ENV_CONFIGPATH="$1"

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT

  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

# Function used for echoing information messages
msg() {
  echo >&2 -e "${1-}"
}

main() {

  # Source the environment variables
  source "$ENV_CONFIGPATH"
  declare -ug FAILED_VERIFICATION
  setup_colors
  pre_check

  for i in "${vms_To_Install[@]}"; do
    # declare a null url variable to be used for installers. reset for every VM
    declare url=""
    # Declare a global verification variable. If any verification fails,
    # variable will be set to Y, and will then notify the user and stop
    # the VM install
    declare -ug FAILED_VERIFICATION="N"

    case $i in
    "WHONIX")
      if [[ "$INSTALL_OVER_TOR" == "Y" ]]; then
        url="$WHONIX_URL_TOR$WHONIX_VERSION/"
      else
        url="$WHONIX_URL_CLEARNET$WHONIX_VERSION/"
      fi
      # Download needed files to build whonix
      download "$url$WHONIX_ARCHIVE" "$WHONIX_ARCHIVE"
      download "$url$WHONIX_ARCHIVE_SIGNED" "$WHONIX_ARCHIVE_SIGNED"
      download "$url$WHONIX_SHA512" "$WHONIX_SHA512"
      download "$url$WHONIX_SHA512_SIGNED" "$WHONIX_SHA512_SIGNED"

      # Verify the files with gpg
      verify_gpg "$WHONIX_ARCHIVE_SIGNED" "$WHONIX_ARCHIVE"
      verify_gpg "$WHONIX_SHA512_SIGNED" "$WHONIX_SHA512"

      # Verify the sha512 hash
      verify_sha512 "$WHONIX_SHA512"

      # Check to see if verifications failed.
      # If N, proceed with install.
      # Else, do not install.
      if [[ "$FAILED_VERIFICATION" == "N" ]]; then
        # Unpack the archive
        unpack "$WHONIX_ARCHIVE"

        # Set up network files
        setup_VMNetwork "$WHONIX_EXTERNAL_NETWORK" "Whonix-External.xml"
        setup_VMNetwork "$WHONIX_INTERNAL_NETWORK" "Whonix-Internal.xml"
        # setup_VMAutoNetwork "Whonix-External.xml"
        # setup_VMAutoNetwork "Whonix-Internal.xml"

        # Set up vm XML files
        sed -i 's|cpuset='\''1'\''>1|cpuset='\''0'\''>2|g' "/mnt$VM_BUILD_DIR/$WHONIX_WORKSTATION_XML"
        setup_VMXML "$WHONIX_GATEWAY_XML" "Whonix-Gateway.xml"
        setup_VMXML "$WHONIX_WORKSTATION_XML" "Whonix-Workstation.xml"

        # Set up qcow2 files
        setup_VMImages "$WHONIX_GATEWAY_QCOW2" "Whonix-Gateway.qcow2"
        setup_VMImages "$WHONIX_WORKSTATION_QCOW2" "Whonix-Workstation.qcow2"
      fi
      ;;
    "KICKSECURE")
      if [[ "$INSTALL_OVER_TOR" == "Y" ]]; then
        url="$KICK_URL_TOR$KICKSECURE_VERSION/"
      else
        url="$KICK_URL_CLEARNET$KICKSECURE_VERSION/"
      fi
      # Download needed files to build kicksecure
      download "$url$KICKSECURE_ARCHIVE" "$KICKSECURE_ARCHIVE"
      download "$url$KICKSECURE_ARCHIVE_SIGNED" "$KICKSECURE_ARCHIVE_SIGNED"
      download "$url$KICKSECURE_SHA512" "$KICKSECURE_SHA512"
      download "$url$KICKSECURE_SHA512_SIGNED" "$KICKSECURE_SHA512_SIGNED"

      # Verify the files with gpg
      verify_gpg "$KICKSECURE_ARCHIVE_SIGNED" "$KICKSECURE_ARCHIVE"
      verify_gpg "$KICKSECURE_SHA512_SIGNED" "$KICKSECURE_SHA512"

      # Verify the sha512 sum
      verify_sha512 "$KICKSECURE_SHA512"

      # Check to see if verifications failed.
      # If N, proceed with install.
      # Else, do not install.
      if [[ "$FAILED_VERIFICATION" == "N" ]]; then
        # Unpack the archive
        unpack "$KICKSECURE_ARCHIVE"

        # Setup xml file
        sed -i -r 's|cpuset='\''1'\''>1|cpuset='\''0'\''>2|g' "/mnt$VM_BUILD_DIR/$KICKSECURE_XML"
        setup_VMXML "$KICKSECURE_XML" "Kicksecure.xml"

        # Setup qcow2 files
        setup_VMImages "$KICKSECURE_QCOW2" "Kicksecure.qcow2"
      fi
      ;;
    esac
  done

  post_check

}

# Used to import keys in VM_SIGNING_KEYS
# Build the VM build directory
# Get list of VM's that were selected by user to
# be installed and set up TOR if indicated by
# user
pre_check() {

  # Get list of VM's indicated by user to be installed.
  # Set a global array of vm's to install
  declare -ga vms_To_Install=()
  declare -u installVM="N"
  # Set associative array for the VM's available to the user to install.
  declare -A vms_available
  vms_available[WHONIX]="$WHONIX"
  vms_available[KICKSECURE]="$KICKSECURE"

  # Loop through associatve array to populate vms_To_Install array
  # If set to Y, then add to vms_To_Install array. Else do nothing.
  for i in "${!vms_available[@]}"; do
    if [[ "${vms_available[$i]}" == "Y" ]]; then
      vms_To_Install+=("$i")
      installVM="Y"
    fi
  done

  if [[ "$installVM" == "Y" ]]; then
    # Import all gpg keys in VM_SIGNING_KEYS
    # For VM's supported by Plague to download
    # If a gpg signing key is present, it will be
    # stored in the git installation directory under
    # /keys/
    for i in "${VM_SIGNING_KEYS[@]}"; do
      gpg --import $i
    done

    # Create a build directory on the host machine
    # so archives and iso's downloaded are garenteed
    # enough room
    mkdir -p "/mnt$VM_BUILD_DIR"

    # Set up TOR on system if user set to use TOR for installs
    if [[ "$INSTALL_OVER_TOR" == "Y" ]]; then
      export SOCKS_PROXY="socks5://127.0.0.1:9050"
    fi

    # Ensure libvirt is installed and needed directories
    # Are available
    for i in "${VM_DIRECTORIES[@]}"; do
      if [[ ! -d "/mnt$i" ]]; then
        msg "${RED}ERROR: ${NOFORMAT}$i is not present on host system."
        die "Exiting build. Can not continue with error"
      fi
    done
  fi

}

# Downloads files from url passed to it
# When calling this function, must pass in
# the URL as the first variable. All vm files will
# be installed to VM_BUILD_DIR and named as
# what is inputed in the second variable when calling downlaod.
download() {

  declare url=$1
  declare fileName=$2

  if [[ "$INSTALL_OVER_TOR" == "Y" ]]; then
    curl --socks5-hostname 127.0.0.1:9050 "$url" -o "/mnt$VM_BUILD_DIR/$fileName"
  else
    curl --tlsv1.2 --proto =https "$url" -o "/mnt$VM_BUILD_DIR/$fileName"
  fi

}

# Used to verify gpg signatures.
# used if a sig file is availabe which
# signed packages installed
# Must pass through the signed file
# and the file that needs to be checked.
verify_gpg() {

  declare signedFile=$1
  declare verifyFile=$2

  if ! gpg --verify-options show-notations --verify "/mnt$VM_BUILD_DIR/$signedFile" "/mnt$VM_BUILD_DIR/$verifyFile"; then
    msg "${RED}ERROR: ${NOFORMAT}$verifyFile could not be verified."
    FAILED_VERIFICATION="Y"
  fi

}

# various funcitons for verifying hashes of VM installed.
verify_md5() {
  
  declare hashFile=$1
  declare -i code
  
  cd "/mnt$VM_BUILD_DIR"
  md5sum -c "$hashFile"
  code=$?
  
  if [[ $code -ne 0 ]]
  then
    msg "${RED}ERROR: ${NOFORMAT}$hashFile could not verify vm."
    FAILED_VERIFICATION="Y"
  fi
  
  cd "$SCRIPT_DIR"

}

verify_sha1() {

  declare hashFile=$1
  declare -i code
  
  cd "/mnt$VM_BUILD_DIR"
  sha1sum -c "$hashFile"
  code=$?
  
  if [[ $code -ne 0 ]]
  then
    msg "${RED}ERROR: ${NOFORMAT}$hashFile could not verify vm."
    FAILED_VERIFICATION="Y"
  fi
  
  cd "$SCRIPT_DIR"

}

verify_sha256() {

  declare hashFile=$1
  declare -i code
  
  cd "/mnt$VM_BUILD_DIR"
  sha256sum -c "$hashFile"
  code=$?
  
  if [[ $code -ne 0 ]]
  then
    msg "${RED}ERROR: ${NOFORMAT}$hashFile could not verify vm."
    FAILED_VERIFICATION="Y"
  fi
  
  cd "$SCRIPT_DIR"

}

verify_sha512() {

  declare hashFile=$1
  declare -i code
  
  cd "/mnt$VM_BUILD_DIR"
  sha512sum -c "$hashFile"
  code=$?
  
  if [[ $code -ne 0 ]]
  then
    msg "${RED}ERROR: ${NOFORMAT}$hashFile could not verify vm."
    FAILED_VERIFICATION="Y"
  fi
  
  cd "$SCRIPT_DIR"

}

# Used to unpack archive files.
# Must pass in the file name to unpack.
# will look in build dir for the file and unpack in
# the same dir
unpack() {

  declare archive=$1

  tar -xvf "/mnt$VM_BUILD_DIR/$archive" -C "/mnt$VM_BUILD_DIR/"

}

# Function used to copy over vm xml files
# Must pass in the file name copying over
# and the name it should be set as
# at copied location. If no name is passed in
# it will auto set it to the origonal name
setup_VMXML() {

  declare fileName=$1
  declare fileName_copied

  if [[ "$2" == "" ]]; then
    fileName_copied=$fileName
  else
    fileName_copied=$2
  fi

  cp -f "/mnt$VM_BUILD_DIR/$fileName" "/mnt$VM_XML_DIR/$fileName_copied"

}

# Function used to copy over network xml files
# used in VM's. Must pass in the file name to be copied
# and what it should be named when copied over.
# If no name passed in as second variable, it will
# auto set it to the origonal name
setup_VMNetwork() {

  declare fileName=$1
  declare fileName_copied

  if [[ "$2" == "" ]]; then
    fileName_copied=$fileName
  else
    fileName_copied=$2
  fi

  cp -f "/mnt$VM_BUILD_DIR/$fileName" "/mnt$VM_NETWORK_XML_DIR/$fileName_copied"

}

# Function used to link a network file to
# autostart. Must pass in the network file's
# name
setup_VMAutoNetwork() {

  declare fileName=$1

  ln -sf "/mnt$VM_NETWORK_XML_DIR/$fileName" "/mnt$VM_AUTOSTART_NETWORK_XML_DIR/$fileName"

}

# Function used to copy over .qcow2 or .iso images
# to be used bo libvirt.
# Must pass in the file to be copied over and the
# name it should be. If no name passed in second variable
# auto selects origonal name
setup_VMImages() {

  declare fileName=$1
  declare fileName_copied

  if [[ "$2" == "" ]]; then
    fileName_copied=$fileName
  else
    fileName_copied=$2
  fi

  mv -f "/mnt$VM_BUILD_DIR/$fileName" "/mnt$VM_IMAGES_DIR/$fileName_copied"

}

post_check() {

  # Delete the VM build directory
  rm -rf "/mnt$VM_BUILD_DIR"

}

main
