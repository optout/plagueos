#!/usr/bin/env bash

###############################
#
#         ###Usage###
# 1. Partition users drive with
#     - Separate boot partition
#     - Encrypted root partition with
#       - btrfs subvolumes
#
###############################

# Set global variable equal to env.cfg absolute path
# This is passed in from plague-install
ENV_CONFIGPATH="$1"

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  #   umount -R "/mnt"
  #   cryptsetup close "/dev/mapper/$CRYPT_MAPPER"
  #   exit
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

# Function used for echoing information messages
msg() {
  echo >&2 -e "${1-}"
}

# Function used for echoing user prompt messages.
prompt() {
  echo >&2 -en "${1-}"
}

# Function used for writing user variables to config file
# calling function. run write_config <the key to look up> <the value to replace>
write_config() {

  # Make sure all keywords are UPPERCASE
  declare -u keyWord="${1-}="
  # Add quotes around value
  declare value="\"${2-}\""

  # Escape special characters that will interfere with sed
  # Or calling variables in env.cfg
  case "$value" in
  *-*)
    value=$(sed "s/\-/\\\-/g" <<<$value)
    ;&
  *'$'*)
    value="'$value'"
    ;;
  *) ;;

  esac

  sed -i -r "s-$keyWord.*-$keyWord$value-g" "$ENV_CONFIGPATH"
  WRITTEN_CONFIGS+=("$keyWord$value")
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

main() {
  source "$ENV_CONFIGPATH"
  #   pre_check
  setup_colors
  prepare_disks
  partition_disks
  encrypt_disk
  format_disks
  mount_parts
  get_uuid
  post_check

}

prepare_disks() {

  declare -a diskParts=()
  declare mountPoints=()
  declare devType
  # Get all partitions related to device
  #   readarray -t diskParts < <(lsblk -nlo NAME,MOUNTPOINT,FSTYPE,PATH /dev/$DEVICE)
  readarray -t diskParts < <(lsblk -nlo PATH /dev/$DEVICE)
  # Force a unmount of all disk partitions
  for i in "${diskParts[@]}"; do
    # Check if there are any mounts on specified diskPartition
    readarray -t mountPoints < <(findmnt -nlo TARGET $i)

    # If there are mount points, unmount it with umount -R.
    # Only do this on first index of array because this will
    # be the parent point point and will grab every child
    # mount point
    if [[ ${#mountPoints[@]} -gt 0 ]]; then
      umount -R ${mountPoints[0]}
    fi

    # If the file system is crypt, close it out using cryptsetup
    devType=$(lsblk -nldo TYPE $i)
    case "$devType" in
    "crypt")
      cryptsetup close $i
      ;;
    *) ;;
    esac
  done

  if [[ "$SECWIPE" == "Y" ]]; then
    # Secure wipe according to cryptosetup FAQ 2.19:
    # https://gitlab.com/cryptsetup/cryptsetup/-/wikis/FrequentlyAskedQuestions#2-setup
    msg "INFORM: Securely Wiping Disk"
    msg "This will take a time. Sit Tight"
    if [[ "$IS_SSD" == "1" ]]; then
      msg "${ORANGE}WARNING:${NOFORMAT} SSD detected. Secure wiping an SSD is unreliable and may result in unwiped data."
      if [[ "$SSD_TYPE" == "nvme" ]]; then
        NVME_SANITIZE=$(nvme id-ctrl /dev/$DEVICE -H | grep "Crypto Erase Sanitize" | awk '{ print $3 }')
        NVME_FORMAT=$(nvme id-ctrl /dev/$DEVICE -H | grep "Format NVM Supported" | awk '{ print $3 }')
        if [[ "$NVME_SANITIZE" != "0" ]]; then
          nvme format /dev/$DEVICE -s 2 -f
        elif [[ "$NVME_FORMAT" == "0x1" ]]; then
          nvme sanitize /dev/$DEVICE --sanact=4 -f
        else
          msg "${RED}ERROR:${NOFORMAT} No crypto erase controller function found."
        fi
      else
        hdparm --user-master u --security-set-pass 1EtdWDTwJJxb /dev/$DEVICE
        hdparm --user-master u --security-erase 1EtdWDTwJJxb /dev/$DEVICE
      fi
    fi
    cryptsetup open --type plain -d /dev/urandom -q /dev/$DEVICE tmp
    set +e
    dd if=/dev/zero of=/dev/mapper/tmp bs=1M
    set -e
    cryptsetup close /dev/mapper/tmp
  else
    dd if=/dev/urandom of=/dev/"$DEVICE" bs=512 count=1
  fi
}

partition_disks() {
  if [[ "$FIRMWARE" == "UEFI" ]]; then
    fdisk /dev/$DEVICE >/dev/null <<EOF
          g
          n
          1

          +512M
          n
          2


          t
          1
          1
          w
EOF
  elif [[ "$FIRMWARE" == "BIOS" ]]; then
    fdisk /dev/$DEVICE >/dev/null <<EOF
          o
          n
          p
          1

          +512M
          n
          p
          2


          t
          1
          83
          t
          2
          83
          a
          1
          w
EOF
  fi
}

encrypt_disk() {

  declare -i extCode

  # Remove the -e flag on set for these commands.
  # Need to catch err codes and act on them for this function.
  # Ex. if user incorrectly verifies enc part password
  # entire script will exit, when user just needs to reenter
  set +e

  # Encrypt disk
  # while loop ensures user correctly enters a password and verifies.
  # Does not break the loop unless exit code is 0
  while :; do
    gpg --batch -q --passphrase '' -r "$GPG_UID" --decrypt "$ENCRYPTED_PASS_FILE" | cryptsetup -q --cipher "$CIPHER" --key-size "$KEY_SIZE" --use-random --pbkdf "$PBKDF" luksFormat /dev/$ENCRYPTED_PART
    extCode=$?
    if [[ $extCode == 0 ]]; then
      break
    else
      msg "${RED}ERROR:${NOFORMAT} luksFormat failed. Re-prompting for password"
      if [[ -e /dev/mapper/"$CRYPT_MAPPER" ]]; then
        cryptsetup luksClose /dev/mapper/"$CRYPT_MAPPER"
      fi
    fi
    echo "$extCode"
  done

  # while loop ensures user correctly enters password.
  # Does not break the loop unless exit code is 0
  while :; do
    gpg --batch -q --passphrase '' -r "$GPG_UID" --decrypt "$ENCRYPTED_PASS_FILE" | cryptsetup -q luksOpen /dev/$ENCRYPTED_PART "$CRYPT_MAPPER"
    extCode=$?
    if [[ $extCode == 0 ]]; then
      break
    else
      msg "${RED}ERROR:${NOFORMAT} luksOpen failed. Re-prompting for password"
      if [[ -e /dev/mapper/"$CRYPT_MAPPER" ]]; then
        cryptsetup luksClose /dev/mapper/"$CRYPT_MAPPER"
      fi
    fi
  done

  set -e
}

format_disks() {
  # Format $CRYPT_MAPPER as btrsfs
  mkfs.btrfs /dev/mapper/"$CRYPT_MAPPER" --checksum blake2

  # Temporarily mount partition and create subvolumes
  mount /dev/mapper/"$CRYPT_MAPPER" /mnt
  for i in "${BTRFS_SUBVOLUMES[@]}"; do
    btrfs subvolume create /mnt/"$i"
  done

  # Unmount btrfs
  umount -R /mnt

  # Format boot
  mkfs.vfat /dev/"$BOOT_PART"
}

mount_parts() {

  declare -la sudoFS=("sys" "dev" "proc")

  # Mount btrfs and all subvolumes
  for i in "${!BTRFS_SUBVOLUMES[@]}"; do
    if ! [[ -d /mnt"${BTRFS_MOUNT_POINTS[$i]}" ]]; then
      mkdir -p /mnt"${BTRFS_MOUNT_POINTS[$i]}"
    fi
    mount -o $BTRFS_MOUNT_OPTIONS,subvol="${BTRFS_SUBVOLUMES[$i]}" /dev/mapper/"$CRYPT_MAPPER" /mnt"${BTRFS_MOUNT_POINTS[$i]}"
  done

  # Mount boot partition
  mkdir /mnt/boot
  mount -o rw,noatime /dev/"$BOOT_PART" /mnt/boot

  # Mount sudo filesystem for chroot
  for i in ${sudoFS[@]}; do
    mkdir "/mnt/$i"
    mount --rbind "/$i" "/mnt/$i"
    mount --make-rslave "/mnt/$i"
  done

}

# Gather UUID's of devices.
get_uuid() {

  declare BOOT_PART_UUID=$(blkid -o value -s UUID /dev/"$BOOT_PART")
  declare ENCRYPTED_PART_UUID=$(blkid -o value -s UUID /dev/"$ENCRYPTED_PART")
  declare CRYPT_MAPPER_UUID=$(blkid -o value -s UUID /dev/mapper/"$CRYPT_MAPPER")

  write_config "BOOT_PART_UUID" "$BOOT_PART_UUID"
  write_config "ENCRYPTED_PART_UUID" "$ENCRYPTED_PART_UUID"
  write_config "CRYPT_MAPPER_UUID" "$CRYPT_MAPPER_UUID"

}

# Check to make sure all partitions were successfully written.
post_check() {

  # Check UUIDs written
  # Initiate variable to use locally
  declare -i errCount=0
  declare fsType_Boot
  declare fsType_Enc
  declare -a mountPoints

  # Loop through the written configs to see if they were
  # Succesfully written to env.cfg.
  # If error, re write config and check again
  for i in "${WRITTEN_CONFIGS[@]}"; do
    if ! grep -q "$i" "$ENV_CONFIGPATH"; then
      msg "${RED}ERROR: ${NOFORMAT}$i not found in env.cfg file"
      errCount+=1
    fi
  done

  # If there are any config errors. die
  if [[ $errCount -gt 0 ]]; then
    msg "${RED}ERROR: ${NOFORMAT}There were ${ORANGE}$errCount${NOFORMAT} configuration errors found."
    die "Exiting script. Can not continue with configuration errors"
  fi

  # Check 2 partitions were created and formatted correctly
  fsType_Boot=$(lsblk -dno FSTYPE "/dev/$BOOT_PART")
  if [[ "$fsType_Boot" != "vfat" ]]; then
    msg "${RED}ERROR: ${NOFORMAT}Boot fs is not partitioned as vfat"
  fi
  fsType_Enc=$(lsblk -dno FSTYPE "/dev/$ENCRYPTED_PART")
  if [[ "$fsType_Enc" != "crypto_LUKS" ]]; then
    msg "${RED}ERROR: ${NOFORMAT}Encrypted part fs is not partitioned as crypto_LUKS"
  fi
  # Check all subvolumes are mounted correctly
  for i in "${!BTRFS_SUBVOLUMES[@]}"; do
    if ! mountpoint -q /mnt"${BTRFS_MOUNT_POINTS[$i]}"; then
      msg "${RED}ERROR: ${NOFORMAT}$I has not been properly mounted."
      die "Exiting installation. Can not proceed with error."
    fi
  done

  # Clean up the gpg keys and encrypted file that contains cryptsetup password.
  # Over write file first
  dd if=/dev/urandom of="$ENCRYPTED_PASS_FILE" bs=1024 count=5
  # Remove file
  rm -rf "$ENCRYPTED_PASS_FILE"
  # Delete the gpg keys used for encryption
  gpg --batch --yes --delete-secret-key "$GPG_FINGERPRINT"
  gpg --batch --yes --delete-key "$GPG_FINGERPRINT"

}

main
