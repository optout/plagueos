#!/usr/bin/env bash

###############################
#
#         ### Usage ###
# 1. Write confgurations from install repo onto host system
#
###############################

# Set global variable equal to env.cfg absolute path
# This is passed in from plague-install
ENV_CONFIGPATH="$1"
declare -a PERMS_LIST

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    # script cleanup here
}

setup_colors() {
    if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
        NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
    else
        NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
    fi
}

die() {
    local msg=$1
    local code=${2-1} # default exit status 1
    msg "$msg"
    exit "$code"
}

# Function used for echoing information messages
msg() {
    echo >&2 -e "${1-}"
}

main() {
    source "$ENV_CONFIGPATH"
    setup_colors
    pre_check
    accounts_config
    create_dirs
    copy_files
    hardened_kern_config
    symlink_services
    symlink_machineid
    pam_config
    fstab_config
    broadwell_config
    securetty_clear
    bin_copy
    runit_copy
    hardened_malloc
    enable_doas
    #usbctl_config
    apparmor_config
    grub_config
}

# Function used as a pre-check to make sure all files are found before starting.
pre_check() {

    # Declare array to store files not found.
    declare -a filesNotFound=()

    # Loop through COPY_FILES array and check that all
    # files are in the build directory
    for i in "${COPY_FILES[@]}"; do
        if [[ ! -f "$SCRIPT_DIR""$BASECONF_DIR""$i" ]]; then
            if [[ ! -d "$SCRIPT_DIR""$BASECONF_DIR""$i" ]]; then
                filesNotFound+=("$i")
            fi
        fi
    done

    # If the filesNotFound array has a length greater than 0
    # A file was not found. Print out file path and die
    if [[ ${#filesNotFound[@]} -gt 0 ]]; then
        for i in "${filesNotFound[@]}"; do
            msg "${RED}ERROR: ${NOFORMAT}$i not found"
        done
        die "Exiting Build: Can not continue with errors"
    fi

}

create_dirs() {
    for i in "${DIR_LIST[@]}"; do
        if [[ ! -d "$i" ]]; then
            mkdir -p "$i"
        fi
    done
}

copy_files() {
    for i in "${COPY_FILES[@]}"; do
        cp -r "$SCRIPT_DIR"$BASECONF_DIR"$i" /mnt"$i"
    done
}

symlink_services() {
    if [[ "$INSTALL_OVER_TOR" == "Y" ]]; then
        SV_DIRS+=("tor")
    fi

    for i in "${SV_DIRS[@]}"; do
        chroot /mnt sh -c "ln -fs /etc/sv/$i /etc/runit/runsvdir/default/"
    done
}


hardened_kern_config() {

    tar -xf "$HKERNARCHIVE" -C "/mnt$HKDIR"
    chroot /mnt sh -c "dracut --kver $KVER --force"

}


symlink_machineid() {

    chroot /mnt sh -c "ln -fs /etc/machine-id /var/lib/dbus/machine-id"

}

pam_config() {
    echo -n "auth optional pam_faildelay.so delay=4000000" >>/mnt/etc/pam.d/system-login
}

fstab_config() {
    sed -i -r 's/UUID=CRYPTUUID+/UUID='"$CRYPT_MAPPER_UUID"'/g' /mnt/etc/fstab
    sed -i -r 's/UUID=BOOT+/UUID='"$BOOT_PART_UUID"'/g' /mnt/etc/fstab
}

accounts_config() {

    declare -i ADMIN_UID

    # Lock root account
    passwd -R /mnt -l root

    # Create system groups
    for i in "${SYSGROUPS[@]}"; do
        if ! grep -q -E "^$i:" /mnt/etc/group; then
            groupadd -R /mnt $i
        fi
    done

    # Remove double quotes from pass
    ADMIN_PASS=${ADMIN_PASS#\"}
    ADMIN_PASS=${ADMIN_PASS%\"}
    # Create admin user
    useradd -R /mnt -d "$ADMIN_HOME_DIR" -s /bin/bash "$ADMIN_NAME"
    usermod -R /mnt -a -G "$ADMIN_GROUPS" "$ADMIN_NAME"
    usermod -R /mnt -p $ADMIN_PASS "$ADMIN_NAME"

    # Set admin UID
    i="0"
    ADMIN_UID=900
    while :; do
        if ! usermod -R /mnt -u "$ADMIN_UID" "$ADMIN_NAME"; then
            ADMIN_UID=$(($ADMIN_UID - 1))
        else
            break
        fi
    done

    # Remove double quotes from pass
    USER_PASS=${USER_PASS#\"}
    USER_PASS=${USER_PASS%\"}

    # Create user account
    useradd -R /mnt -d "$USER_HOME_DIR" -s /bin/bash "$USER_NAME"
    usermod -R /mnt -a -G "$USER_GROUPS" "$USER_NAME"
    usermod -R /mnt -p "$USER_PASS" "$USER_NAME"

}

broadwell_config() {
    if [[ $GPU == "INTEL" ]]; then
        if lspci | grep 'VGA' | grep -iq 'Broadwell'; then
            sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="[^"]*/& intel_iommu=igfx_off/' "$FULLFILEPATH"
        fi
    fi
}

securetty_clear() {
    rm /mnt/etc/securetty
    touch /mnt/etc/securetty
}

bin_copy() {
    find "/mnt$BINDIR" -type d -exec chmod ugo=rx,ugo-w {} \;
    find "/mnt$BINDIR" -type f -exec chmod ugo=rx,ugo-w {} \;

    BINPROC=($(ls "/mnt$BINDIR"))

    for i in ${BINPROC[@]}; do
        chroot /mnt sh -c "ln -s /plague/bin/$i /usr/bin/$i"
    done

}

runit_copy() {
    find "/mnt$RUNITDIR" -type d -exec chmod ugo=rx,ugo-w {} \;
    find "/mnt$RUNITDIR" -type f -exec chmod ugo=rx,ugo-w {} \;

    RUNITPROC=($(ls "/mnt$RUNITDIR"))

    for i in ${RUNITPROC[@]}; do
        chroot /mnt sh -c "ln -s /plague/runit/$i /etc/runit/runsvdir/default/"
    done
    if [[ "$HIDE_HARDWARE_ENABLED" == "N" ]]; then
        touch "/mnt/plague/runit/plague-hide-hardware-info/down"
    fi
}

hardened_malloc() {

      git clone https://github.com/GrapheneOS/hardened_malloc.git
      cd hardened_malloc

      make

      install out/libhardened_malloc.so "/mnt$HMFILE"
      chmod ugo=rx,ugo-w "/mnt$HMFILE"
      chmod ugo=r,ugo-w "/mnt$HMPRELOAD"

      cd "$SCRIPT_DIR"
  }

enable_doas() {
    if xbps-query -r /mnt sudo; then
        xbps-remove -r /mnt -y sudo
        rm -f /mnt/bin/sudo && rm -f /mnt/usr/bin/sudo
    fi
    chroot /mnt sh -c "ln -s /usr/bin/doas /usr/bin/sudo"
}

usbctl_config() {
    chroot /mnt sh -c "usbctl protect"
}

apparmor_config() {
    LINESINPROFILE=$(wc -l <"$VIRTAAHELPERFILE")

    sed -i "`wc -l < $VIRTAAHELPERFILE`i\\  #adding in execute permissions for apparmor_parser\\" "$VIRTAAHELPERFILE"
    sed -i "`wc -l < $VIRTAAHELPERFILE`i\\  /usr/bin/apparmor_parser Ux,\\" "$VIRTAAHELPERFILE"
    sed -i -r 's/leaseshelper m+/'"leaseshelper mr"'/g' "$DNSMASQAAFILE"

    # Set immutable flag to AA profiles
    # chattr +i "$VIRTAAHELPERFILE"
    # chattr +i "$DNSMASQAAFILE"

}

grub_config() {
    if [[ "$FIRMWARE" == "UEFI" ]]; then
        chroot /mnt sh -c "/usr/bin/grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=Void /dev/$DEVICE"
    elif [[ "$FIRMWARE" == "BIOS" ]]; then
        chroot /mnt sh -c "/usr/bin/grub-install /dev/$DEVICE"
    else
        msg "${RED}ERROR: ${NOFORMAT}Firmware not set."
        die "Exiting Build: Can not continue with errors"
    fi

    sed -i -r 's/#CRYPTUUID#+/'"$ENCRYPTED_PART_UUID"'/g' "/mnt$GRUBFILE"
    sed -i -r 's/#ROOTUUID#+/'"$CRYPT_MAPPER_UUID"'/g' "/mnt$GRUBFILE"

    chroot /mnt sh -c "/usr/bin/grub-mkconfig -o /boot/grub/grub.cfg"
    xbps-reconfigure -r /mnt -fa
    chroot /mnt sh -c "/usr/bin/update-grub"
}

main
