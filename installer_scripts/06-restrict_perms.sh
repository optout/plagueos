#!/usr/bin/env bash

###############################
#
#         ###Usage###
# 1. Restrict host systems files and directories to least privilaged access.
#
###############################

# Set global variable equal to env.cfg absolute path
# This is passed in from plague-install
ENV_CONFIGPATH="$1"
declare -a PERMS_LIST

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

# Function used for echoing information messages
msg() {
  echo >&2 -e "${1-}"
}

main() {

  source "$ENV_CONFIGPATH"
  setup_colors
  set_bulk_permissions
  set_file_permission

}

set_bulk_permissions() {

  # delcare an array to hold the file and directory
  # octal perm values
  # file will be located at index 0, directory index 1
  declare -a perm_arr

  # Loop through indexs of SET_BULK_DIR_PERM
  # Index values are directory paths
  for i in ${!SET_BULK_DIR_PERM[@]}; do
    # If index is a directory, go through permission set
    # else, throw error and continue
    if [[ -d "/mnt$i" ]]; then
      # set deliminator to a comma
      # and split array value into file and dir perms
      IFS=','
      read -ra perm_arr <<<"${SET_BULK_DIR_PERM[$i]}"

      # Find all files in directory and set it to the file
      # permission value. then find all directories in
      # directory and set it to the directory permission
      # value
      find "/mnt$i" -type f -exec chmod --preserve-root ${perm_arr[0]} {} \;
      find "/mnt$i" -type d -exec chmod --preserve-root ${perm_arr[1]} {} \;
    else
      die "${RED}ERROR: ${NOFORMAT} $i is not a directory. Skipping"
    fi
  done

}

set_file_permission() {

    # Set permissions on admin home dir
    # chroot /mnt sh -c "chown -R $ADMIN_NAME:$ADMIN_NAME $ADMIN_HOME_DIR/"
    find "/mnt$ADMIN_HOME_DIR/" -type d -exec chmod u=rwx,go-rwx {} \;
    find "/mnt$ADMIN_HOME_DIR/" -type f -exec chmod u=rw,go-rwx {} \;
    find "/mnt$ADMIN_HOME_DIR/" -type d -exec chown "$ADMIN_NAME":"$ADMIN_NAME" {} \;
    find "/mnt$ADMIN_HOME_DIR/" -type f -exec chown "$ADMIN_NAME":"$ADMIN_NAME" {} \;

    # Set perms on user home dir
    # chroot /mnt sh -c "chown -R $USER_NAME:$USER_NAME $USER_HOME_DIR/"
    find "/mnt$USER_HOME_DIR/" -type d -exec chmod u=rwx,go-rwx {} \;
    find "/mnt$USER_HOME_DIR/" -type f -exec chmod u=rw,go-rwx {} \;
    find "/mnt$USER_HOME_DIR/" -type d -exec chown "$USER_NAME":"$USER_NAME" {} \;
    find "/mnt$USER_HOME_DIR/" -type f -exec chown "$USER_NAME":"$USER_NAME" {} \;

  # Loop through the index values of SET_FILE_PERM
  # associative array.
  # The index values are paths to the files
  for i in ${!SET_FILE_PERM[@]}; do
    # If index is a file, changer permission
    # else, throw error
    if [[ -f "/mnt$i" ]]; then
      chmod --preserve-root ${SET_FILE_PERM[$i]} "/mnt$i"
    else
      die "${RED}ERROR: ${NOFORMAT} /mnt$i is not a file. Skipping"
    fi
  done

}

main
