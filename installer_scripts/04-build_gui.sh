#!/usr/bin/env bash

###############################
#
#         ###Usage###
# 1. Install packages needed for selected GUI environment
#
###############################

ENV_CONFIGPATH="$1"

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT

  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

# Function used for echoing information messages
msg() {
  echo >&2 -e "${1-}"
}

main() {

  source "$ENV_CONFIGPATH"
  setup_colors

  declare -g gui_working_dir
  declare -ga gui_dirs=()
  declare -ga gui_files=()
  declare -ga gui_services=()
  declare -ga gui_groups=()

  case $GUI_ENVIRONMENT in
  "CLI") ;;

  "DE")
    gui_working_dir=$DE_WORKING_DIR
    gui_dirs=(${GNOME_DIRS[@]})
    gui_files=(${GNOME_FILES[@]})
    pre_check
    check_dirs
    copy_files
    gui_services=(${GNOME_SERVICES[@]})
    start_services
    reconfigure
    post_check
    ;;
  esac
}

# Function used as a pre-check to make sure all files are found before starting.
pre_check() {

  # Declare array to store files not found.
  declare -a filesNotFound=()

  # Loop through COPY_FILES array and check that all
  # files are in the build directory
  for i in "${gui_files[@]}"; do
    if [[ ! -f "$SCRIPT_DIR""$gui_working_dir""$i" ]]; then
      if [[ ! -d "$SCRIPT_DIR""$gui_working_dir""$i" ]]; then
        filesNotFound+=("$i")
      fi
    fi
  done

  #   filesNotFound_len=${#filesNotFound[@]}
  # If the filesNotFound array has a length greater than 0
  # A file was not found. Print out file path and die
  if [[ ${#filesNotFound[@]} -gt 0 ]]; then
    for i in "${filesNotFound[@]}"; do
      msg "${RED}ERROR: ${NOFORMAT}$i not found"
    done
    die "Exiting Build: Can not continue with errors"
  fi

}

# Check if dir exists on host system. If not create
check_dirs() {

  for i in "${gui_dirs[@]}"; do
    if [[ ! -d "/mnt$i" ]]; then
      mkdir -p "/mnt$i"
    fi
  done

}

# copy all GUI config files to proper locations
copy_files() {

  for i in "${gui_files[@]}"; do
    cp -r "$SCRIPT_DIR""$gui_working_dir""$i" "/mnt$i"
  done

}

# Start any services needed by GUI
start_services() {

  for i in "${gui_services[@]}"; do
    chroot /mnt sh -c "ln -sf /etc/sv/$i /etc/runit/runsvdir/default/"
  done

}

# Add user to any groups needed by GUI
add_groups() {

  for i in "${gui_groups[@]}"; do
    usermod -R /mnt -a -G $i "$USER_NAME"
  done

}

# Used for 

reconfigure() {

  xbps-reconfigure -r /mnt -fa

}

post_check() {

  # Check if dirs present
  for i in "${gui_dirs[@]}"; do
    if [[ ! -d "/mnt$i" ]]; then
      msg "${RED}ERROR: ${NOFORMAT}$i not found"
      die "Exiting build. Can not continue with configuration errors"
    fi
  done

  # Check if config files present
  for i in "${gui_files[@]}"; do
    if [[ ! -f "/mnt$i" ]]; then
      msg "${RED}ERROR: ${NOFORMAT}$i not found"
      die "Exiting build. Can not continue with configuration errors"
    fi
  done

  # Check if services started
  for i in "${gui_services[@]}"; do
    if [[ ! -L "/mnt/etc/runit/runsvdir/default/$i" ]]; then
      msg "${RED}ERROR: ${NOFORMAT}$i service not linked"
      die "Exiting build. Can not continue with configuration errors"
    fi
  done

  # Check if groups added
  if [[ ${#gui_groups[@]} -gt 0 ]]; then
    for i in "${gui_groups[@]}"; do
      chkGroup=$(grep "^$i:.*$" /mnt/etc/group | cut -d: -f4 | grep $USER_NAME)
      if [[ $chkGroup == "" ]]; then
        msg "${RED}ERROR: ${NOFORMAT}$USER_NAME not a member of $i"
        die "Exiting build. Can not continue with configuration errors"
      fi
    done
  fi

}

main
