#!/usr/bin/env bash

###############################
#
#         ###Usage###
# 1. Unmount partitions
# 2. Close encrypted partitoin
# 3. Reconfigure the system
# 4. Reboot OS.
#
###############################

ENV_CONFIGPATH="$1"

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT

  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

# Function used for echoing information messages
msg() {
  echo >&2 -e "${1-}"
}

# Function used for echoing user prompt messages.
prompt() {
  echo >&2 -en "${1-}"
}

main() {

  source "$ENV_CONFIGPATH"
  setup_colors
  reconfigure
  unmount
  close_crypt

}

reconfigure() {

  # Do a final system reconfiguration before ending
  xbps-reconfigure -r /mnt -fa

}

unmount() {

  umount -R /mnt
  
}

close_crypt() {

  cryptsetup close /dev/mapper/"$CRYPT_MAPPER"
  
}

main
