#!/usr/bin/env bash

###############################
#
#         ###Usage###
# 1. Install the base system on users partitioned drive
# 2. Install UEFI/BIOS firmware
# 3. Install the CPU/GPU firmware
# 4. Install the GUI/CLI packages
#
###############################

ENV_CONFIGPATH="$1"

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    #   exit
    # script cleanup here
}

setup_colors() {
    if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
        NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
    else
        NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
    fi
}

die() {
    local msg=$1
    local code=${2-1} # default exit status 1
    msg "$msg"
    exit "$code"
}

# Function used for echoing information messages
msg() {
    echo >&2 -e "${1-}"
}

main() {
    source "$ENV_CONFIGPATH"
    setup_colors
    check_tor
    copy_keys
    install_base
    check_install
}

check_tor() {
    if [[ "$INSTALL_OVER_TOR" == "Y" ]]; then
        REPO="$REPO_TOR"
        sv start tor
        declare -g SOCKS_PROXY="socks5://127.0.0.1:9050"
    else
        REPO="$REPO_CLEAR_NET"
    fi
}

copy_keys() {

    mkdir -p /mnt/var/db/xbps/keys
    cp -r /var/db/xbps/keys/* /mnt/var/db/xbps/keys/

}

install_base() {
    declare -a PACKAGES
    PACKAGES=(${BASE_PACKAGES[@]})

    if [[ "$INSTALL_OVER_TOR" == "Y" ]]; then
        PACKAGES+=("tor")
    fi

    # Add UEFI/BIOS firmware packages
    if [[ "$FIRMWARE" == "UEFI" ]]; then
        for i in "${UEFI_FIRMWARE_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
    elif [[ "$FIRMWARE" == "BIOS" ]]; then
        for i in "${BIOS_FIRMWARE_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
    else
        msg "${RED}ERROR: ${NOFORMAT}No Firmware detected."
        die "Exiting build. Can not continue with error"
    fi

    # Add CPU firmware packages
    case $CPU in
    "INTEL")
        for i in "${INTEL_CPU_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
        ;;
    "AMD")
        for i in "${AMD_CPU_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
        ;;
    *)
        msg "${RED}ERROR: ${NOFORMAT}No CPU selected for package install."
        die "Exiting build. Can not continue with error"
        ;;
    esac

    # Add GPU firmware packages
    case $GPU in
    "INTEL")
        for i in "${INTEL_GPU_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
        ;;
    "AMD")
        for i in "${AMD_GPU_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
        ;;
    "NVIDIA")
        for i in "${NVIDIA_GPU_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
        ;;
    "NONE") ;;

    *)
        msg "${RED}ERROR: ${NOFORMAT}No GPU selected for package install."
        die "Exiting build. Can not continue with error"
        ;;

    esac

    # Install CLI/GUI packages
    case "$GUI_ENVIRONMENT" in
    "CLI")
        for i in "${CLI_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
        ;;
#    "WM")
#        for i in "${WM_PACKAGES[@]}"; do
#            PACKAGES+=("$i")
#        done
#        ;;
    "DE")
        for i in "${DE_PACKAGES[@]}"; do
            PACKAGES+=("$i")
        done
        ;;
    *)
        msg "${RED}ERROR: ${NOFORMAT}No Environment selected for package install."
        die "Exiting build. Can not continue with error"
        ;;

    esac

    # Install packages
    if [[ "$INSTALL_OVER_TOR" == "Y" ]]; then
      for i in "${PACKAGES[@]}"; do
          XBPS_ARCH=$XBPS_ARCH SOCKS_PROXY=$SOCKS_PROXY xbps-install -Sy -r /mnt -R "$REPO" "$i"
      done
    else 
      for i in "${PACKAGES[@]}"; do
          XBPS_ARCH=$XBPS_ARCH xbps-install -Sy -r /mnt -R "$REPO" "$i"
      done
    fi
}

check_install() {

    for i in "${PACKAGES[@]}"; do
        if ! xbps-query "$i"; then
            msg "${RED}ERROR: ${NOFORMAT}$i was not installed."
            die "Exiting. Can not continue with error."
        fi
    done

    # Reconfigure system (sanity check)
    xbps-reconfigure -r /mnt -fa

}

main
